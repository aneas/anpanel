#ifndef INCLUDE_SHADOW_H
#define INCLUDE_SHADOW_H


#include <math.h>

#include "surface.h"
#include "x.h"


class Shadow {
public:
	X::XScreen screen;
	int x, y;
	size_t width, height;
	Window window;
	GC gc;
	Surface * background;

public:
	Shadow() {
		screen = X::get_screen(Config::get_int("screen"));

		x = screen.x;
		y = (Config::get("position") == "top") ? screen.y + Config::get_int("height") : (screen.y + screen.height - Config::get_int("height") - Config::get_int("shadow_size"));
		width = screen.width;
		height = Config::get_int("shadow_size");

		window = XCreateSimpleWindow(X::display, screen.root, 0, 0, width, height, 0, 0, 0);
		X::set_window_decoration(window, 0);
		X::set_window_desktop(window, -1);
		X::set_window_type(window, X::_NET_WM_WINDOW_TYPE_DESKTOP);
		X::select_input(X::display, window, ExposureMask);
		XMapWindow(X::display, window);
		XMoveResizeWindow(X::display, window, x, y, width, height);

		gc = XCreateGC(X::display, window, 0, NULL);

		background = NULL;
		update_background();
	}


	void invalidate() {
		paint();
	}


	void paint() {
		if(background != NULL)
			background->draw_to(window, gc, 0, 0, 0, 0, width, height);
	}


	void update_background() {
		if(background != NULL) {
			delete background;
			background = NULL;
		}

		size_t shadow_size = Config::get_int("shadow_size");
		if(shadow_size > 50)
			shadow_size = 50;
		Color shadow_color = Config::get_color("shadow_color");

		if(shadow_size > 0) {
			XImage * ximage = X::get_desktop_background(screen.xscreen, x, y, width, height);
			if(ximage != NULL) {
				background = new Surface(ximage);

				if(Config::get("position") == "top")
					background->vertical_gradient(shadow_color, shadow_color & 0x00FFFFFF);
				else
					background->vertical_gradient(shadow_color & 0x00FFFFFF, shadow_color);
			}
		}

		int corner_size = Config::get_int("shadow_corner_size", 0);
		if(corner_size > 0) {
			Pixels * corner_left = new Pixels(corner_size, corner_size);
			Pixels * corner_right = new Pixels(corner_size, corner_size);
			for(int y = 0; y < corner_size; y++) {
				for(int x = 0; x < corner_size; x++) {
					float distance = sqrt(x * x + y * y);
					Color c;
					if(distance >= corner_size)
						c = Color(0, 0, 0, 255);
					else if(distance >= corner_size - 1)
						c = Color(0, 0, 0, 255 * (distance - corner_size - 1));
					(*corner_left)(corner_size - 1 - x, y) = c;
					(*corner_right)(x, y) = c;
				}
			}

			if(Config::get("position") == "top") {
				corner_left->flip_vertical_inplace();
				corner_right->flip_vertical_inplace();
				background->draw(*corner_left, 0, 0, 0, 0, corner_left->width, corner_left->height);
				background->draw(*corner_right, 0, 0, width - corner_right->width, 0, corner_right->width, corner_right->height);
			}
			else {
				background->draw(*corner_left, 0, 0, 0, height - corner_left->height, corner_left->width, corner_left->height);
				background->draw(*corner_right, 0, 0, width - corner_right->width, height - corner_right->height, corner_right->width, corner_right->height);
			}
		}
	}


	void on_event(XEvent * ev) {
		if(ev->xany.window == window && ev->type == Expose)
			invalidate();

		// desktop background changed
		if(ev->type == PropertyNotify && X::is_root_window(ev->xproperty.window) && ev->xproperty.atom == X::_XROOTPMAP_ID) {
			update_background();
			invalidate();
		}
	}
};


#endif
