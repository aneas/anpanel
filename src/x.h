#ifndef INCLUDE_X_H
#define INCLUDE_X_H


#include <string>
#include <vector>

#include <X11/Xlib.h>
#include <X11/Xutil.h>
#include <X11/Xatom.h>
#include <X11/extensions/Xinerama.h>

#include "pixels.h"


bool XNextEventTimeout(Display * display, XEvent * event, unsigned long milliseconds) {
	// check directly first as some events may be queued
	XSync(display, False);
	if(XPending(display)) {
		XNextEvent(display, event);
		return true;
	}

	struct timeval timeout;
	timeout.tv_sec = milliseconds / 1000;
	timeout.tv_usec = (milliseconds % 1000) * 1000;

	fd_set rset;
	FD_ZERO(&rset);
	FD_SET(ConnectionNumber(display), &rset);
	if(select(ConnectionNumber(display) + 1, &rset, NULL, NULL, &timeout) > 0) {
		XNextEvent(display, event);
		return true;
	}

	return false;
}


namespace X {


struct XScreen {
	size_t id;
	int xscreen;
	Window root;
	int x, y;
	size_t width, height;
};


Display * display;
Window root_window;

std::vector<XScreen> screens;

const long _NET_WM_STATE_REMOVE = 0;
const long _NET_WM_STATE_ADD = 1;
const long _NET_WM_STATE_TOGGLE = 2;


Atom _NET_WM_WINDOW_TYPE_DESKTOP;
Atom _NET_WM_WINDOW_TYPE_DOCK;
Atom _NET_WM_DESKTOP;
Atom _NET_WM_STRUT;
Atom _MOTIF_WM_HINTS;
Atom WM_HINTS;
Atom _NET_WM_WINDOW_TYPE;
Atom _NET_CURRENT_DESKTOP;
Atom _NET_NUMBER_OF_DESKTOPS;
Atom _NET_CLIENT_LIST;
Atom _NET_CLIENT_LIST_STACKING;
Atom WM_STATE;
Atom _XROOTPMAP_ID;
Atom _NET_ACTIVE_WINDOW;
Atom _NET_MOVERESIZE_WINDOW;
Atom _NET_WM_STATE;
Atom _NET_WM_STATE_SHADED;
Atom _NET_WM_STATE_SKIP_PAGER;
Atom _NET_WM_STATE_SKIP_TASKBAR;
Atom _NET_WM_STATE_DEMANDS_ATTENTION;
Atom _NET_WM_VISIBLE_NAME;
Atom _NET_WM_NAME;
Atom _WM_NAME;
Atom _NET_WM_ICON;
Atom UTF8_STRING;
Atom _NET_SYSTEM_TRAY_OPCODE;
Atom MANAGER;
Atom _NET_CLOSE_WINDOW;
Atom _NET_WM_STATE_MAXIMIZED_VERT;
Atom _NET_WM_STATE_MAXIMIZED_HORZ;
Atom _NET_FRAME_EXTENTS;


int error_handler(Display * display, XErrorEvent * event) {
	char buffer[1024];
	XGetErrorText(display, event->error_code, buffer, sizeof(buffer) - 1);

	printf("X Error: %s\n", buffer);
	return 0;
}


void initialize() {
	display = XOpenDisplay(NULL);
	if(display == NULL) {
		fprintf(stderr,
			"ERROR: could not open X display\n"
			"     - is an Xserver running? if not, start it :-)\n"
			"     - is the environment variable DISPLAY set correctly?\n"
			"       if not, try `export DISPLAY=':0.0'`\n");
		exit(1);
	}

	XSetErrorHandler(error_handler);

	_NET_WM_WINDOW_TYPE_DESKTOP = XInternAtom(display, "_NET_WM_WINDOW_TYPE_DESKTOP", False);
	_NET_WM_WINDOW_TYPE_DOCK = XInternAtom(display, "_NET_WM_WINDOW_TYPE_DOCK", False);
	_NET_WM_DESKTOP = XInternAtom(display, "_NET_WM_DESKTOP", False);
	_NET_WM_STRUT = XInternAtom(display, "_NET_WM_STRUT", False);
	_MOTIF_WM_HINTS = XInternAtom(display, "_MOTIF_WM_HINTS", False);
	WM_HINTS = XInternAtom(display, "WM_HINTS", False);
	_NET_WM_WINDOW_TYPE = XInternAtom(display, "_NET_WM_WINDOW_TYPE", False);
	_NET_CURRENT_DESKTOP = XInternAtom(display, "_NET_CURRENT_DESKTOP", False);
	_NET_NUMBER_OF_DESKTOPS = XInternAtom(display, "_NET_NUMBER_OF_DESKTOPS", False);
	_NET_CLIENT_LIST = XInternAtom(display, "_NET_CLIENT_LIST", False);
	_NET_CLIENT_LIST_STACKING = XInternAtom(display, "_NET_CLIENT_LIST_STACKING", False);
	WM_STATE = XInternAtom(display, "WM_STATE", False);
	_NET_WM_STATE_SHADED = XInternAtom(display, "_NET_WM_STATE_SHADED", False);
	_XROOTPMAP_ID = XInternAtom(display, "_XROOTPMAP_ID", False);
	_NET_ACTIVE_WINDOW = XInternAtom(display, "_NET_ACTIVE_WINDOW", False);
	_NET_MOVERESIZE_WINDOW = XInternAtom(display, "_NET_MOVERESIZE_WINDOW", False);
	_NET_WM_STATE = XInternAtom(display, "_NET_WM_STATE", False);
	_NET_WM_STATE_SKIP_PAGER = XInternAtom(display, "_NET_WM_STATE_SKIP_PAGER", False);
	_NET_WM_STATE_SKIP_TASKBAR = XInternAtom(display, "_NET_WM_STATE_SKIP_TASKBAR", False);
	_NET_WM_STATE_DEMANDS_ATTENTION = XInternAtom(display, "_NET_WM_STATE_DEMANDS_ATTENTION", False);
	_NET_WM_VISIBLE_NAME = XInternAtom(display, "_NET_WM_VISIBLE_NAME", False);
	_NET_WM_NAME = XInternAtom(display, "_NET_WM_NAME", False);
	_WM_NAME = XInternAtom(display, "_WM_NAME", False);
	_NET_WM_ICON = XInternAtom(display, "_NET_WM_ICON", False);
	UTF8_STRING = XInternAtom(display, "UTF8_STRING", False);
	_NET_SYSTEM_TRAY_OPCODE = XInternAtom(display, "_NET_SYSTEM_TRAY_OPCODE", False);
	MANAGER = XInternAtom(display, "MANAGER", False);
	_NET_CLOSE_WINDOW = XInternAtom(display, "_NET_CLOSE_WINDOW", False);
	_NET_WM_STATE_MAXIMIZED_VERT = XInternAtom(display, "_NET_WM_STATE_MAXIMIZED_VERT", False);
	_NET_WM_STATE_MAXIMIZED_HORZ = XInternAtom(display, "_NET_WM_STATE_MAXIMIZED_HORZ", False);
	_NET_FRAME_EXTENTS = XInternAtom(display, "_NET_FRAME_EXTENTS", False);

	root_window = XRootWindow(display, XDefaultScreen(display));

	if(XineramaIsActive(display)) {
		int screen_count;
		XineramaScreenInfo *xinscreens = XineramaQueryScreens(display, &screen_count);

		if(xinscreens != NULL) {
			for(size_t i = 0; i < (size_t)screen_count; i++) {
				XScreen screen;
				screen.id = i;
				screen.xscreen = XDefaultScreen(display);
				screen.root = XRootWindow(display, screen.xscreen);
				screen.x = xinscreens[i].x_org;
				screen.y = xinscreens[i].y_org;
				screen.width = xinscreens[i].width;
				screen.height = xinscreens[i].height;
				screens.push_back(screen);
			}
			return;
		}
	}

	XScreen screen;
	screen.id = 0;
	screen.xscreen = XDefaultScreen(display);
	screen.root = XRootWindow(display, screen.xscreen);
	screen.x = 0;
	screen.y = 0;
	screen.width = XDisplayWidth(display, screen.xscreen);
	screen.height = XDisplayHeight(display, screen.xscreen);
	screens.push_back(screen);
}


Atom atom(std::string name) {
	return XInternAtom(display, name.c_str(), False);
}


bool is_root_window(Window window) {
	return (root_window == window);
}


// dont forget to XFree(prop_return); !
void *get_window_property(Window window, Atom property, Atom type, int *number) {
	Atom actual_type_return;
	int actual_format_return;
	unsigned long nitems_return;
	unsigned long bytes_after_return;
	unsigned char *prop_return = 0;

	XGetWindowProperty(display, window, property, 0, -1, False, type, &actual_type_return, &actual_format_return, &nitems_return, &bytes_after_return, &prop_return);

	if(number != NULL)
		*number = nitems_return;

	return prop_return;
}


void * get_screen_property(int xscreen, Atom property, Atom type, int * number) {
	return get_window_property(XRootWindow(display, xscreen), property, type, number);
}


int get_window_integer(Window window, Atom property) {
	int *value = (int *)get_window_property(window, property, XA_CARDINAL, NULL);
	if(value != NULL) {
		int number = *value;
		XFree(value);
		return number;
	}
	return 0;
}


void XRestackBelow(Display * display, Window window, Window below) {
	int window_count;
	Window * windows = (Window *)get_screen_property(XDefaultScreen(display), X::_NET_CLIENT_LIST_STACKING, XA_WINDOW, &window_count);
	for(int i = 0; i < window_count; i++) {
		if(windows[i] == below)
			XRaiseWindow(display, window);
		if(windows[i] != window)
			XRaiseWindow(display, windows[i]);
	}
	XFree(windows);
/*	Window windows[2];
	windows[0] = below;
	windows[1] = window;
	XRestackWindows(display, windows, 2);*/
}


Window XNextWindowAbove(Display * display, int xscreen, Window window) {
	int window_count;
	Window * windows = (Window *)get_screen_property(xscreen, X::_NET_CLIENT_LIST_STACKING, XA_WINDOW, &window_count);
	for(int i = 0; i + 1 < window_count; i++) {
		if(windows[i] == window) {
			Window above = windows[i + 1];
			XFree(windows);
			return above;
		}
	}
	XFree(windows);
	return None;
}


void set_window_property(Window window, Atom property, Atom type, unsigned char * value, int length) {
	XChangeProperty(display, window, property, type, 32, PropModeReplace, value, length);
	XFlush(display);
}


int get_current_desktop(int xscreen) {
	return get_window_integer(XRootWindow(display, xscreen), _NET_CURRENT_DESKTOP);
}


int get_desktop_count(int xscreen) {
	return get_window_integer(XRootWindow(display, xscreen), _NET_NUMBER_OF_DESKTOPS);
}


int get_screen_count() {
	return XScreenCount(display);
}


void reserve_screen_margin(Window window, unsigned long left, unsigned long right, unsigned long top, unsigned long bottom) {
	unsigned long strut[4] = {left, right, top, bottom};
	set_window_property(window, _NET_WM_STRUT, XA_CARDINAL, (unsigned char *)strut, 4);
}


std::string get_window_title(Window window) {
	char * ctitle = (char *)get_window_property(window, _NET_WM_VISIBLE_NAME, UTF8_STRING, NULL);
	if(ctitle != NULL) {
		std::string title(ctitle);
		XFree(ctitle);
		return title;
	}
	return strdup("");
}

Pixels * get_window_icon(Window window) {
	int size;
	long * data = (long *)get_window_property(window, _NET_WM_ICON, XA_CARDINAL, &size);
	if(size > 2) {
		Pixels * pixels;

#ifdef _LP64
		uint32_t * new_data = new uint32_t[data[0] * data[1]];
		for(long i = 0; i < data[0] * data[1]; i++)
			new_data[i] = data[i + 2];
		pixels = new Pixels(data[0], data[1], (Color *)new_data);
#else
		pixels = new Pixels(data[0], data[1], (Color *)data + 2);
#endif

		XFree(data);
		return pixels;
	}
	return NULL;
}

int get_window_desktop(Window window) {
	return get_window_integer(window, _NET_WM_DESKTOP);
}


#ifndef UrgencyHint
#  define UrgencyHint (1L << 8)
#endif

bool is_window_urgent(Window window) {
	XWMHints * hints = XGetWMHints(X::display, window);
	if(hints != NULL) {
		if((hints->flags & UrgencyHint) != 0) {
			XFree(hints);
			return true;
		}
		XFree(hints);
	}
	return false;
}


void set_window_decoration(Window window, int decoration) {
	long prop[5] = {decoration ? 0 : 2, 0, 0, 0, 0};
	set_window_property(window, _MOTIF_WM_HINTS, _MOTIF_WM_HINTS, (unsigned char *)prop, 5);
}


void set_window_desktop(Window window, int desktop) {
	set_window_property(window, _NET_WM_DESKTOP, XA_CARDINAL, (unsigned char *)&desktop, 1);
}


void set_window_type(Window window, Atom type) {
	set_window_property(window, _NET_WM_WINDOW_TYPE, XA_ATOM, (unsigned char *)&type, 1);
}


XImage *get_desktop_background(int xscreen, int x, int y, int width, int height) {
	Pixmap *pixmap = (Pixmap *)get_window_property(XRootWindow(display, xscreen), _XROOTPMAP_ID, XA_PIXMAP, NULL);
	if(pixmap != NULL) {
		XImage *image = XGetImage(display, *pixmap, x, y, width, height, AllPlanes, ZPixmap);
		XFree(pixmap);
		return image;
	}
	return NULL;
}


void send_event(int xscreen, Window window, Atom atom, long data0 = 0, long data1 = 0, long data2 = 0, long data3 = 0, long data4 = 0) {
	XEvent event;
	event.xclient.type = ClientMessage;
	event.xclient.serial = 0;
	event.xclient.send_event = True;
	event.xclient.display = display;
	event.xclient.window = window;
	event.xclient.message_type = atom;

	event.xclient.format = 32;
	event.xclient.data.l[0] = data0;
	event.xclient.data.l[1] = data1;
	event.xclient.data.l[2] = data2;
	event.xclient.data.l[3] = data3;
	event.xclient.data.l[4] = data4;

	XSendEvent(display, XRootWindow(display, xscreen), False, SubstructureRedirectMask | SubstructureNotifyMask, &event);
}


bool get_border_dimensions(Window window, int * width, int * height) {
	int count;
	long * borders = (long *)get_window_property(window, _NET_FRAME_EXTENTS, XA_CARDINAL, &count);
	if(count == 4) {
		*width = borders[0] + borders[1];
		*height = borders[2] + borders[3];
		return true;
	}
	return false;
}


void XMoveResizeWindowDecoration(int xscreen, Window window, int x, int y, int width, int height) {
	int w, h;
	if(get_border_dimensions(window, &w, &h)) {
		width -= w;
		height -= h;
	}
	XMoveResizeWindow(display, window, x, y, width, height);

	// this moves the window to [x - decor.left, y - decor.top, width + decor.left + decor.right, height + decor.top + decor.bottom]
	//send_event(xscreen, window, _NET_MOVERESIZE_WINDOW, 0xF00 | StaticGravity, x, y, width, height);
}


void activate_window(int xscreen, Window window) {
	send_event(xscreen, window, _NET_ACTIVE_WINDOW, 2);
}


void XExposeWindow(Display * display, Window window) {
	XEvent event;
	event.xexpose.type = Expose;
	event.xexpose.serial = 0;
	event.xexpose.send_event = True;
	event.xexpose.display = display;
	event.xexpose.window = window;
	event.xexpose.x = 0;
	event.xexpose.y = 0;
	event.xexpose.width = 999999;
	event.xexpose.height = 999999;
	event.xexpose.count = 1;
	XSendEvent(display, window, False, ExposureMask, &event);
}


void iconify_window(int xscreen, Window window) {
	XIconifyWindow(display, window, xscreen);
}


void uniconify_window(Window window) {
	XMapWindow(display, window);
}


Window get_active_window(int xscreen) {
	Window *window = (Window *)get_window_property(XRootWindow(display, xscreen), _NET_ACTIVE_WINDOW, XA_WINDOW, NULL);
	if(window != NULL) {
		Window ret = *window;
		XFree(window);
		return ret;
	}
	return 0;
}


int is_window_active(int xscreen, Window window) {
	return (get_active_window(xscreen) == window);
}


int is_window_iconified(Window window) {
	unsigned long *value = (unsigned long *)get_window_property(window, WM_STATE, WM_STATE, NULL);
	if(value != NULL) {
		int ret = (*value == IconicState);
		XFree(value);
		return ret;
	}
	return 0;
}


void select_input(Display * display, Window window, long mask) {
	XWindowAttributes attributes;
	XGetWindowAttributes(display, window, &attributes);
	XSelectInput(display, window, mask | attributes.your_event_mask);
}


XScreen get_screen(size_t number) {
	if(number >= screens.size())
		number = screens.size() - 1;

	return screens[number];
}


XScreen get_screen_at(int x, int y) {
	for(size_t i = 0; i < screens.size(); i++)
		if(screens[i].x <= x && screens[i].x + (int)screens[i].width > x && screens[i].y <= y && screens[i].y + (int)screens[i].height > y)
			return screens[i];

	size_t nearest_distance = (size_t)-1;
	size_t nearest_screen = 0;
	for(size_t i = 0; i < screens.size(); i++) {
		size_t distance = 0;
		if(x < screens[i].x)
			distance = screens[i].x - x;
		else if(x >= screens[i].x + (int)screens[i].width)
			distance = x - (screens[i].x + (int)screens[i].width);
		else if(y < screens[i].y)
			distance = screens[i].y - y;
		else if(y >= screens[i].y + (int)screens[i].height)
			distance = y - (screens[i].y + (int)screens[i].height);

		if(distance < nearest_distance) {
			nearest_distance = distance;
			nearest_screen = i;
		}
	}
	return screens[nearest_screen];
}


}


#endif
