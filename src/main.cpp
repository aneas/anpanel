#include "pixels.h"

#include <stdio.h>
#include <stdlib.h>
#include <signal.h>

#include "config.h"
#include "x.h"
#include "tasks.h"
#include "panel.h"
#include "shadow.h"

#include "widgets/spacer.h"
#include "widgets/launcher.h"
#include "widgets/taskbar.h"
#include "widgets/clock.h"
#include "widgets/systray.h"
#include "widgets/rootmenu.h"


const char * VERSION = "0.6";
Panel * panel = NULL;
Shadow * shadow = NULL;


void usage() {
	printf("Usage: anpanel [options]\n");
	printf("Options:\n");
	printf("  -c, --config filename   load an alternative config file\n");
	printf("  -h, --help              print this help\n");
	printf("  -V, --version           print version\n");
	printf("  setting=value           override config setting\n");
}


void signal_handler(int sig) {
	if(panel != NULL)
		delete panel;
	if(shadow != NULL)
		delete shadow;
	exit(EXIT_SUCCESS);
}


#ifdef DEBUG
int break_here(Display * display, XErrorEvent * event) {
	int a = 5;
	int b = 0;
	return a /= b;
}
#endif


int main(int argc, char ** argv) {
	signal(SIGHUP, signal_handler);
	signal(SIGTERM, signal_handler);
	signal(SIGINT, signal_handler);
	signal(SIGQUIT, signal_handler);

	bool config_loaded = false;
	for(int i = 1; i < argc; i++) {
		if(strcmp(argv[i], "-h") == 0 || strcmp(argv[i], "--help") == 0) {
			usage();
			exit(0);
		}
		else if(strcmp(argv[i], "-c") == 0 || strcmp(argv[i], "--config") == 0) {
			if(i + 1 < argc) {
				config_loaded = true;
				i++;
				if(!Config::load_file(argv[i])) {
					fprintf(stderr, "Error: could not load the config file `%s`\n", argv[i]);
					exit(1);
				}
				argv[i][0] = '-'; // so Config::load_arguments will ignore the parameter
			}
		}
		else if(strcmp(argv[i], "-V") == 0 || strcmp(argv[i], "--verbose") == 0) {
			printf("anPanel %s\n", VERSION);
			exit(0);
		}
	}

	if(!config_loaded) {
		char path[PATH_MAX];
		snprintf(path, PATH_MAX, "%s/anpanel/anpanelrc", getenv("XDG_CONFIG_HOME"));
		config_loaded = Config::load_file(path);
	}

	if(!config_loaded) {
		char path[PATH_MAX];
		snprintf(path, PATH_MAX, "%s/.config/anpanel/anpanelrc", getenv("HOME"));
		config_loaded = Config::load_file(path);
	}

	if(!config_loaded)
		config_loaded = Config::load_file(SYSCONFDIR "/xdg/anpanel/anpanelrc");

	if(!config_loaded) {
		fprintf(stderr, "Error: no config file found in\n");
		fprintf(stderr, "       ~/.config/anpanel/anpanelrc\n");
		fprintf(stderr, "       " SYSCONFDIR "/xdg/anpanel/anpanelrc\n");
		exit(1);
	}

	Config::load_arguments(argc, argv);

	X::initialize();
#ifdef DEBUG
	XSynchronize(X::display, True);
	XSetErrorHandler(break_here);

#endif
	for(int i = 0; i < XScreenCount(X::display); i++)
		XSelectInput(X::display, XRootWindow(X::display, i), PropertyChangeMask);

	panel = new Panel();
	shadow = new Shadow();

	std::vector<Configuration> cfgs = Config::global_config.get_subconfigs("plugins");
	for(size_t i = 0; i < cfgs.size(); i++) {
		if(cfgs[i].get("type") == "spacer")
			panel->add(new Spacer(panel, cfgs[i]));
		else if(cfgs[i].get("type") == "launcher")
			panel->add(new Launcher(panel, cfgs[i]));
		else if(cfgs[i].get("type") == "taskbar")
			panel->add(new Taskbar(panel, cfgs[i]));
		else if(cfgs[i].get("type") == "clock")
			panel->add(new Clock(panel, cfgs[i]));
		else if(cfgs[i].get("type") == "systray")
			panel->add(new Systray(panel, cfgs[i]));
		else if(cfgs[i].get("type") == "rootmenu")
			panel->add(new RootMenu(panel, cfgs[i]));
	}

	Tasks::update_all_tasks();
	Tasks::update_active_task();


	while(1) {
		XEvent ev;
		if(XNextEventTimeout(X::display, &ev, 30000)) {
	/*		if(ev.type == DestroyNotify)
				printf("Event: XDestroyWindow (window=%08X)\n", (unsigned int)ev.xdestroywindow.window);
			if(ev.type == UnmapNotify)
				printf("Event: XUnmap (window=%08X)\n", (unsigned int)ev.xunmap.window);
			if(ev.type == PropertyNotify)
				printf("Event: XProperty (window=%08X, atom=%s, %s)\n", (unsigned int)ev.xproperty.window, XGetAtomName(X::display, ev.xproperty.atom), (ev.xproperty.state == PropertyNewValue) ? "changed" : "deleted");
			if(ev.type == ConfigureNotify)
				printf("Event: XConfigure (window=%08X, x=%d, y=%d, width=%d, height=%d, border_width=%d)\n", (unsigned int)ev.xconfigure.window, ev.xconfigure.x, ev.xconfigure.y, ev.xconfigure.width, ev.xconfigure.height, ev.xconfigure.border_width);
	*/
			Tasks::on_event(&ev);
			panel->on_event(&ev);
			shadow->on_event(&ev);
		}
		else
			panel->on_timer();
	}

	delete shadow;
	delete panel;

	XCloseDisplay(X::display);

	return 0;
}
