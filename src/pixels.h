#ifndef INCLUDE_PIXELS_H
#define INCLUDE_PIXELS_H


#include <string>
#include <png.h>

#include "color.h"


struct Pixels {
	size_t width, height;
	bool opaque;
	Color * data;


	Pixels(size_t width, size_t height) : width(width), height(height), opaque(false) {
		data = new Color[width * height];
		memset(data, 0, width * height * sizeof(Color));
	}


	Pixels(size_t width, size_t height, const Color * data) : width(width), height(height) {
		this->data = new Color[width * height];
		memcpy(this->data, data, width * height * sizeof(Color));
		update_opaque_status();
	}


	Pixels(const Pixels & p) : width(p.width), height(p.height), opaque(p.opaque) {
		data = new Color[width * height];
		memcpy(data, p.data, width * height * sizeof(Color));
	}


	Pixels(std::string filename) {
		width = height = 0;
		data = NULL;
		opaque = false;

		// Open the file
		FILE *infile = fopen(filename.c_str(), "rb");
		if(!infile) {
			fprintf(stderr, "error: could not open `%s'\n", filename.c_str());
			width   = height = 1;
			opaque  = false;
			data    = new Color[1];
			data[0] = Color(0, 0, 0, 0);
			return;
		}

		// Check for the 8-byte signature
		char sig[8];
		fread(sig, 1, 8, infile);
		if(!png_check_sig((unsigned char *)sig, 8)) {
			fclose(infile);
			return;
		}

		// set up the PNG struct
		png_structp png_ptr = png_create_read_struct(PNG_LIBPNG_VER_STRING, NULL, NULL, NULL);
		if(!png_ptr) {
			// out of memory
			fclose(infile);
			return;
		}

		// set up the PNG info struct
		png_infop info_ptr = png_create_info_struct(png_ptr);
		if(!info_ptr) {
			// out of memory
			png_destroy_read_struct(&png_ptr, (png_infopp)NULL, (png_infopp)NULL);
			fclose(infile);
			return;
		}

		// block to handle libpng errors, then check whether the PNG file had a bKGD chunk
		if(setjmp(png_jmpbuf(png_ptr))) {
			png_destroy_read_struct(&png_ptr, &info_ptr, NULL);
			fclose(infile);
			return;
		}

		// stores the file stream pointer in the png_ptr struct
		png_init_io(png_ptr, infile);

		// we already checked the 8 signature bytes
		png_set_sig_bytes(png_ptr, 8);

		// read all the info up to the image data
		png_read_info(png_ptr, info_ptr);

		// extract some important information
		int bit_depth, color_type;
		png_get_IHDR(png_ptr, info_ptr, (png_uint_32 *)&width, (png_uint_32 *)&height, &bit_depth, &color_type, NULL, NULL, NULL);

		// Set up some transforms
		if(bit_depth > 8)
			png_set_strip_16(png_ptr);
		if(color_type == PNG_COLOR_TYPE_GRAY || color_type == PNG_COLOR_TYPE_GRAY_ALPHA)
			png_set_gray_to_rgb(png_ptr);
		if(color_type == PNG_COLOR_TYPE_PALETTE)
			png_set_palette_to_rgb(png_ptr);
		if((color_type & PNG_COLOR_MASK_ALPHA) == 0)
			png_set_add_alpha(png_ptr, 0xFFFF, PNG_FILLER_AFTER);
		png_set_bgr(png_ptr);

		// Update the png info struct
		png_read_update_info(png_ptr, info_ptr);

		// Rowsize in bytes
		unsigned int rowbytes = png_get_rowbytes(png_ptr, info_ptr);

		// Allocate the row pointer buffer
		png_bytepp row_pointers = new png_bytep[height];

		// create the resulting pixel buffer
		data = new Color[width * height];

		// set the individual row_pointers to point at the correct offsets
		unsigned long i;
		for(i = 0; i < height; ++i)
			row_pointers[i] = (png_bytep)data + i * rowbytes;

		// read the whole image
		png_read_image(png_ptr, row_pointers);

		// check for opacity
		update_opaque_status();

		// clean up
		delete[] row_pointers;
		png_destroy_read_struct(&png_ptr, &info_ptr, NULL);
		fclose(infile);
	}


	~Pixels() {
		if(data != NULL)
			delete[] data;
	}


	void update_opaque_status() {
		opaque = true;
		for(size_t i = 0; i < height * height; i++) {
			if(data[i].a != 0xFF) {
				opaque = 0;
				break;
			}
		}
	}


	Color & operator()(size_t x, size_t y) {
		return data[y * width + x];
	}


	const Color & operator()(size_t x, size_t y) const {
		return data[y * width + x];
	}


	// taken from libgd
	Color get_area(float left, float top, float right, float bottom) const {
		double spixels = 0;
		double red = 0.0, green = 0.0, blue = 0.0, alpha = 0.0;
		double sy = top;
		do {
			double yportion;
			if((int)sy == (int)top) {
				yportion = 1.0 - (sy - (int)sy);
				if(yportion > bottom - top)
					yportion = bottom - top;
				sy = (int)sy;
			}
			else if(sy == (int)bottom)
				yportion = bottom - (int)bottom;
			else
				yportion = 1.0;

			double sx = left;
			do {
				double xportion;
				if((int)sx == (int)left) {
					xportion = 1.0 - (sx - (int)sx);
					if(xportion > right - left)
						xportion = right - left;
					sx = (int)sx;
				}
				else if(sx == (int)right)
					xportion = right - (int)right;
				else
					xportion = 1.0;
				double pcontribution = xportion * yportion;
				Color c = get_pixel_repeat((int)sx, (int)sy);
				red += c.r * pcontribution;
				green += c.g * pcontribution;
				blue += c.b * pcontribution;
				alpha += c.a * pcontribution;
				spixels += pcontribution;
				sx += 1.0;
			} while(sx < right);
			sy += 1.0;
		} while(sy < bottom);

		if(spixels == 0.0)
			spixels = 1.0;

		return Color(std::min(red / spixels, 255.0), std::min(green / spixels, 255.0), std::min(blue / spixels, 255.0), std::min(alpha / spixels, 255.0));
	}


	// nearest neighbour
	Pixels resize_fast(size_t dest_width, size_t dest_height) const {
		Pixels dest(dest_width, dest_height);
		for(size_t y = 0; y < dest_height; y++) {
			for(size_t x = 0; x < dest_width; x++) {
				size_t src_x = (float)x * width / dest_width;
				size_t src_y = (float)y * height / dest_height;
				dest.data[y * dest_width + x] = data[src_y * width + src_x];
			}
		}
		return dest;
	}


	Pixels resize(size_t dest_width, size_t dest_height) const {
		Pixels dest(dest_width, dest_height);
		for(size_t y = 0; y < dest_height; y++) {
			float top = (float)y * (float)height / (float)dest_height;
			float bottom = (float)(y + 1) * (float)height / (float)dest_height;
			for(size_t x = 0; x < dest_width; x++) {
				float left = (float)x * (float)width / (float)dest_width;
				float right = (float)(x + 1) * (float)width / (float)dest_width;
				dest.data[y * dest_width + x] = get_area(left, top, right, bottom);
			}
		}
		return dest;
	}


	Color get_pixel_repeat(int x, int y) const {
		x = (x < 0) ? 0 : ((x >= (int)width) ? (int)width - 1 : x);
		y = (y < 0) ? 0 : ((y >= (int)height) ? (int)height - 1 : y);
		return data[y * width + x];
	}


	Color fold(int left, int top, double * matrix, size_t width, size_t height) {
		double r = 0, g = 0, b = 0, a = 0;
		for(int y = top; y < top + (int)height; y++) {
			for(int x = left; x < left + (int)width; x++) {
				Color c = get_pixel_repeat(x, y);
				r += matrix[0] * c.r;
				g += matrix[0] * c.g;
				b += matrix[0] * c.b;
				a += matrix[0] * c.a;
				matrix++;
			}
		}
		return Color(r, g, b, a);
	}


	void flip_vertical_inplace() {
		for(size_t y = 0; 2 * y + 1 < height; y++)
			for(size_t x = 0; x < width; x++)
				std::swap(data[y * width + x], data[(height - y - 1) * width + x]);
	}


	void flip_horizontal_inplace() {
		for(size_t y = 0; y < height; y++)
			for(size_t x = 0; 2 * x + 1 < width; x++)
				std::swap(data[y * width + x], data[y * width + width - x - 1]);
	}


	void fill(Color color) {
		if(color.a == 0xFF) {
			for(size_t y = 0; y < height; y++)
				for(size_t x = 0; x < width; x++)
					operator()(x, y) = color;
		}
		else if(color.a != 0x00) {
			for(size_t y = 0; y < height; y++)
				for(size_t x = 0; x < width; x++)
					operator()(x, y) = color.over(operator()(x, y));
		}
	}


	void glow(float factor) {
		if(factor != 1.0) {
			for(size_t y = 0; y < height; y++)
				for(size_t x = 0; x < width; x++)
					operator()(x, y) = operator()(x, y).glow(factor);
		}
	}
};


#endif
