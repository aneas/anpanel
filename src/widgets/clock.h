#ifndef INCLUDE_WIDGETS_CLOCK_H
#define INCLUDE_WIDGETS_CLOCK_H


#include <time.h>

#include "../panel.h"


class Clock : public Widget {
public:
	Clock(Panel * panel, const Configuration & config) : Widget(panel), mouse_inside(false) {
		desired_width = config.get_int("width", 100);
		background = config.get_color("background");
		background_hover = config.get_color("background_hover");
		time_font = XftFontOpenName(X::display, panel->screen.xscreen, config.get("time_font").c_str());
		date_font = XftFontOpenName(X::display, panel->screen.xscreen, config.get("date_font").c_str());
		color.color = config.get_color("color");
		command = config.get("command");
	}


	void paint_background(Surface & surface) {
		Color fill_color = mouse_inside ? background_hover : background;
		surface.fill_rect(fill_color, x, y, width, height);
	}


	void paint(XftDraw * xft_draw) {
		static const char * weekday_names[] = { "So", "Mo", "Tu", "We", "Th", "Fr", "Sa" };
		time_t timestamp = time(0);
		struct tm * stime = localtime(&timestamp);
		char buffer[64];
		XftFont * font;
		if(!mouse_inside) {
			sprintf(buffer, "%02d:%02d", stime->tm_hour, stime->tm_min);
			font = time_font;
		}
		else {
			sprintf(buffer, "%s, %02d.%02d.%02d", weekday_names[stime->tm_wday], stime->tm_mday, stime->tm_mon + 1, stime->tm_year % 100);
			font = date_font;
		}

		XGlyphInfo extents;
		XftTextExtentsUtf8(X::display, font, (unsigned char *)buffer, strlen(buffer), &extents);
		XftDrawStringUtf8(xft_draw, &color, font, x + (width - extents.width) / 2, y + (height + extents.height) / 2, (unsigned char *)buffer, strlen(buffer));
	}


	void on_mouse_enter(int x, int y, unsigned int state) {
		mouse_inside = true;
		panel->invalidate();
	}


	void on_mouse_leave(unsigned int state) {
		mouse_inside = false;
		panel->invalidate();
	}


	void on_timer(unsigned int state) {
		panel->invalidate();
	}


	void on_mouse_down(int x, int y, int button, unsigned int state) {
		launch(command);
	}


private:
	XftFont * time_font, * date_font;
	XftColor color;
	Color background, background_hover;
	bool mouse_inside;
	std::string command;
};


#endif
