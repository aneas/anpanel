#ifndef INCLUDE_WIDGETS_SYSTRAY_H
#define INCLUDE_WIDGETS_SYSTRAY_H


#include "../panel.h"


#define SYSTEM_TRAY_REQUEST_DOCK    0
#define SYSTEM_TRAY_BEGIN_MESSAGE   1
#define SYSTEM_TRAY_CANCEL_MESSAGE  2


class Systray : public Widget {
public:
	Window selection_owner;
	std::vector<Window> icons;
	Atom atom_selection;
	int icon_size;
	int icon_margin;


	Systray(Panel * panel, const Configuration & config) : Widget(panel) {
		selection_owner = 0;
		desired_width = 0;
		icon_size = config.get_int("size", 16);
		icon_margin = config.get_int("margin", 0);

		// check if a systray already exists
		char atom_name[64];
		sprintf(atom_name, "_NET_SYSTEM_TRAY_S%d", XDefaultScreen(X::display));
		atom_selection = X::atom(atom_name);
		if(XGetSelectionOwner(X::display, atom_selection) != 0) {
			printf("cannot acquire systray selection\n");
			return;
		}

		// create a selection owner window
		selection_owner = XCreateSimpleWindow(X::display, panel->window, -1, -1, 1, 1, 0, 0, 0);

		// acquire the manager selection _NET_SYSTEM_TRAY_Sn
		XSetSelectionOwner(X::display, atom_selection, selection_owner, CurrentTime);

		// tell the root window about it
		XEvent event;
		event.xclient.type = ClientMessage;
		event.xclient.serial = 0;
		event.xclient.send_event = True;
		event.xclient.display = X::display;
		event.xclient.window = XRootWindow(X::display, 0);
		event.xclient.message_type = X::MANAGER;
		event.xclient.format = 32;
		event.xclient.data.l[0] = CurrentTime;
		event.xclient.data.l[1] = atom_selection;
		event.xclient.data.l[2] = selection_owner;
		event.xclient.data.l[3] = 0;
		event.xclient.data.l[4] = 0;
		XSendEvent(X::display, XRootWindow(X::display, 0), False, StructureNotifyMask, &event);
	}


	~Systray() {
		for(size_t i = 0; i < icons.size(); i++)
			XReparentWindow(X::display, icons[i], X::root_window, 0, 0);
		XFlush(X::display);

		if(selection_owner != 0) {
			XSetSelectionOwner(X::display, atom_selection, None, CurrentTime);
			XDestroyWindow(X::display, selection_owner);
		}
	}


	void add_icon(Window window) {
		icons.push_back(window);
		XReparentWindow(X::display, window, panel->window, 0, 0);
		XSelectInput(X::display, window, ExposureMask | StructureNotifyMask);
		XMapRaised(X::display, window);
		update_desired_width();
	}


	void remove_icon(Window window) {
		for(size_t i = 0; i < icons.size(); i++) {
			if(icons[i] == window) {
				icons.erase(icons.begin() + i);
				update_desired_width();
				break;
			}
		}
	}


	void update_desired_width() {
		int new_desired_width = icons.size() * icon_size;
		if(icons.size() > 0)
			new_desired_width += (icons.size() - 1) * icon_margin;

		if(desired_width != new_desired_width) {
			desired_width = new_desired_width;
			panel->pack();
		}
	}


	void pack() {
		for(size_t i = 0; i < icons.size(); i++)
			XMoveResizeWindow(X::display, icons[i], x + i * (icon_size + icon_margin), y + (height - icon_size) / 2, icon_size, icon_size);
	}


	void on_event(XEvent * event) {
		switch(event->type) {
			case ClientMessage:
				if(event->xclient.window == selection_owner && event->xclient.message_type == X::_NET_SYSTEM_TRAY_OPCODE && event->xclient.data.l[1] == SYSTEM_TRAY_REQUEST_DOCK)
					add_icon((Window)event->xclient.data.l[2]);
				break;

			case DestroyNotify:
				remove_icon(event->xdestroywindow.window);
				break;

			case ReparentNotify:
				if(event->xreparent.parent != selection_owner)
					remove_icon(event->xreparent.window);
				break;

			case SelectionClear:
				if(event->xselectionclear.window == selection_owner) {
					// we lost the systray selection, we're all goint to die
					icons.clear();
					update_desired_width();
				}
				break;

			case ConfigureNotify:
				for(size_t i = 0; i < icons.size(); i++) {
					if(icons[i] == event->xconfigure.window) {
						pack();
						break;
					}
				}
				break;

			case PropertyNotify:
				if(X::is_root_window(event->xproperty.window)) {
					// desktop background changed
					if(event->xproperty.atom == X::_XROOTPMAP_ID) {
						// at least some systray icons use CopyFromParent as their background.
						// desktop background changes, our window background changes and all icons have to be redrawn
						for(size_t i = 0; i < icons.size(); i++)
							X::XExposeWindow(X::display, icons[i]);
					}
				}
				break;
		}
	}
};


#endif
