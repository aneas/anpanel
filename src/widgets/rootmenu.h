#ifndef INCLUDE_WIDGETS_ROOTMENU_H
#define INCLUDE_WIDGETS_ROOTMENU_H


#include <unistd.h>

#include "../panel.h"


class RootMenu : public Widget {
public:
	RootMenu(Panel * panel, const Configuration & config) : Widget(panel), hovered(false) {
		desired_width = config.get_int("width", 20);
		background = config.get_color("background");
		background_hover = config.get_color("background_hover");
		icon_size = config.get_int("icon_size", 16);

		if(desired_width < icon_size)
			desired_width = icon_size;

		Pixels * big_icon = new Pixels(config.get("icon"));
		icon = new Pixels(big_icon->resize(icon_size, icon_size));
		delete big_icon;

		if(config.isset("icon_hover")) {
			big_icon = new Pixels(config.get("icon_hover"));
			icon_hover = new Pixels(big_icon->resize(icon_size, icon_size));
			delete big_icon;
		}
		else if(config.isset("icon_hover_glow"))
			icon_hover = new Pixels(*icon);
		else
			icon_hover = NULL;

		if(config.isset("icon_hover_glow"))
			icon_hover->glow(config.get_float("icon_hover_glow", 1.0));
	}


	void paint_background(Surface & surface) {
		Color fill_color = hovered ? background_hover : background;
		surface.fill_rect(fill_color, x, y, width, height);

		if(hovered && icon_hover != NULL)
			surface.draw(*icon_hover, 0, 0, x + (width - icon_hover->width) / 2, y + (height - icon_hover->height) / 2, icon_hover->width, icon_hover->height);
		else if(icon != NULL)
			surface.draw(*icon, 0, 0, x + (width - icon->width) / 2, y + (height - icon->height) / 2, icon->width, icon->height);
	}


	void on_mouse_enter(int x, int y, unsigned int state) {
		hovered = true;
		panel->invalidate();
	}


	void on_mouse_leave(unsigned int state) {
		hovered = false;
		panel->invalidate();
	}


	void on_mouse_up(int x, int y, int button, unsigned int state) {
		X::XScreen screen = X::get_screen(Config::get_int("screen"));

		int menu_x = screen.x + this->x;
		int menu_y = (Config::get("position") == "top") ? (screen.y + Config::get_int("height") + 1) : (screen.y + screen.height - Config::get_int("height"));

		XEvent ev;
		ev.xbutton.type = ButtonPress;
		ev.xbutton.serial = 0;
		ev.xbutton.send_event = True;
		ev.xbutton.display = X::display;
		ev.xbutton.window = X::root_window;
		ev.xbutton.root = X::root_window;
		ev.xbutton.subwindow = None;
		ev.xbutton.time = CurrentTime;
		ev.xbutton.x = menu_x;
		ev.xbutton.y = menu_y;
		ev.xbutton.x_root = menu_x;
		ev.xbutton.y_root = menu_y;
		ev.xbutton.state = 0;
		ev.xbutton.button = Button3;
		ev.xbutton.same_screen = True;
		XSendEvent(X::display, X::root_window, False, ButtonPressMask, &ev);
		XFlush(X::display);
	}


private:
	Pixels * icon, * icon_hover;
	int icon_size;
	Color background, background_hover;
	bool hovered;
};


#endif
