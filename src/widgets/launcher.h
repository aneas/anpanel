#ifndef INCLUDE_WIDGETS_LAUNCHER_H
#define INCLUDE_WIDGETS_LAUNCHER_H


#include <unistd.h>
#include <sys/stat.h> /* umask */

#include "../panel.h"


class Launcher : public Widget {
public:
	Launcher(Panel * panel, const Configuration & config) : Widget(panel), hovered(false) {
		desired_width = config.get_int("width", 20);
		command = config.get("command");
		background = config.get_color("background");
		background_hover = config.get_color("background_hover");
		icon_size = config.get_int("icon_size", 16);

		if(desired_width < icon_size)
			desired_width = icon_size;

		Pixels * big_icon = new Pixels(config.get("icon"));
		icon = new Pixels(big_icon->resize(icon_size, icon_size));
		delete big_icon;

		if(config.isset("icon_hover")) {
			big_icon = new Pixels(config.get("icon_hover"));
			icon_hover = new Pixels(big_icon->resize(icon_size, icon_size));
			delete big_icon;
		}
		else if(config.isset("icon_hover_glow"))
			icon_hover = new Pixels(*icon);
		else
			icon_hover = NULL;

		if(config.isset("icon_hover_glow"))
			icon_hover->glow(config.get_float("icon_hover_glow", 1.0));
	}


	void paint_background(Surface & surface) {
		Color fill_color = hovered ? background_hover : background;
		surface.fill_rect(fill_color, x, y, width, height);

		if(hovered && icon_hover != NULL)
			surface.draw(*icon_hover, 0, 0, x + (width - icon_hover->width) / 2, y + (height - icon_hover->height) / 2, icon_hover->width, icon_hover->height);
		else if(icon != NULL)
			surface.draw(*icon, 0, 0, x + (width - icon->width) / 2, y + (height - icon->height) / 2, icon->width, icon->height);
	}


	void on_mouse_enter(int x, int y, unsigned int state) {
		hovered = true;
		panel->invalidate();
	}


	void on_mouse_leave(unsigned int state) {
		hovered = false;
		panel->invalidate();
	}


	void on_mouse_down(int x, int y, int button, unsigned int state) {
		launch(command);
	}


private:
	Pixels * icon, * icon_hover;
	int icon_size;
	Color background, background_hover;
	std::string command;
	bool hovered;
};


#endif
