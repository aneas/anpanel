#ifndef INCLUDE_WIDGETS_TASKBAR_H
#define INCLUDE_WIDGETS_TASKBAR_H


#include <map>

#include "../panel.h"
#include "../tasks.h"


enum TaskbarAction {
	ACTION_NONE,
	ACTION_ACTIVATE_ICONIFY,
	ACTION_ACTIVATE,
	ACTION_ICONIFY,
	ACTION_MAXIMIZE,
	ACTION_MAXIMIZE_LEFT,
	ACTION_MAXIMIZE_RIGHT,
	ACTION_SHOW,
	ACTION_HIDE,
	ACTION_ROLL,
	ACTION_ROLLUP,
	ACTION_ROLLDOWN,
	ACTION_CLOSE
};


class TaskbarTask : public TaskListener {
public:
	Task * task;
	Panel * panel;
	int x, y;
	size_t width, height;
	bool last_button;
	int icon_size;

	bool hovered;

	XftFont * normal_font;
	XftFont * active_font;
	XftFont * iconified_font;
	XftFont * attention_font;

	int border_width;
	std::vector<Color> border_color;

	Configuration config;

	TaskbarAction left_click_action, left_double_click_action, left_ctrl_click_action, right_click_action, right_double_click_action, right_ctrl_click_action, middle_click_action, middle_double_click_action, wheel_up_action, wheel_down_action;


	TaskbarTask(Task * task, Panel * panel, const Configuration & config) : task(task), panel(panel), last_button(false), hovered(false), config(config) {
		task->add_listener(this);

		normal_font = XftFontOpenName(X::display, panel->screen.xscreen, config.get("normal_font").c_str());
		active_font = XftFontOpenName(X::display, panel->screen.xscreen, config.get("active_font").c_str());
		iconified_font = XftFontOpenName(X::display, panel->screen.xscreen, config.get("iconified_font").c_str());
		attention_font = XftFontOpenName(X::display, panel->screen.xscreen, config.get("attention_font").c_str());

		left_click_action = stringToAction(config.get("on_left_click"), ACTION_ACTIVATE);
		left_double_click_action = stringToAction(config.get("on_left_double_click"), ACTION_MAXIMIZE);
		left_ctrl_click_action = stringToAction(config.get("on_left_ctrl_click"), ACTION_MAXIMIZE_LEFT);
		right_click_action = stringToAction(config.get("on_right_click"), ACTION_ICONIFY);
		right_double_click_action = stringToAction(config.get("on_right_double_click"));
		right_ctrl_click_action = stringToAction(config.get("on_right_ctrl_click"), ACTION_MAXIMIZE_RIGHT);
		middle_click_action = stringToAction(config.get("on_middle_click"), ACTION_HIDE);
		middle_double_click_action = stringToAction(config.get("on_middle_double_click"));
		wheel_up_action = stringToAction(config.get("on_wheel_up"), ACTION_ROLLUP);
		wheel_down_action = stringToAction(config.get("on_wheel_down"), ACTION_ROLLDOWN);

		icon_size = config.get_int("icon_size", 16);

		border_width = config.get_int("border_width", 1);
		border_color = config.get_gradient("background");
	}


	TaskbarAction stringToAction(const std::string & str, TaskbarAction default_action = ACTION_NONE) {
		if(str == "none")
			return ACTION_NONE;
		else if(str == "activate_iconify")
			return ACTION_ACTIVATE_ICONIFY;
		else if(str == "activate")
			return ACTION_ACTIVATE;
		else if(str == "iconify")
			return ACTION_ICONIFY;
		else if(str == "maximize")
			return ACTION_MAXIMIZE;
		else if(str == "maximize_left")
			return ACTION_MAXIMIZE_LEFT;
		else if(str == "maximize_right")
			return ACTION_MAXIMIZE_RIGHT;
		else if(str == "show")
			return ACTION_SHOW;
		else if(str == "hide")
			return ACTION_HIDE;
		else if(str == "roll")
			return ACTION_ROLL;
		else if(str == "rollup")
			return ACTION_ROLLUP;
		else if(str == "rolldown")
			return ACTION_ROLLDOWN;
		else if(str == "close")
			return ACTION_CLOSE;
		return default_action;
	}


	void on_task_title_changed(Task * task) {
		panel->invalidate();
	}


	void on_task_icon_changed(Task * task) {
		panel->invalidate();
	}


	void on_task_state_changed(Task * task) {
		panel->invalidate();
	}


	std::string XftTextMaxWidth(XftDraw * draw, XftFont * font, std::string text, std::string dots, size_t max_width) {
		XGlyphInfo extents;
		size_t length = text.size();
		XftTextExtentsUtf8(X::display, font, (unsigned char *)text.c_str(), length, &extents);

		if(extents.width <= max_width)
			return text;

		XftTextExtents8(X::display, font, (unsigned char *)dots.c_str(), dots.size(), &extents);
		size_t dots_width = extents.width;

		do {
			length--;
			XftTextExtentsUtf8(X::display, font, (unsigned char *)text.c_str(), length, &extents);
		} while(extents.width + dots_width > max_width && length > 0);

		return text.substr(0, length) + dots;
	}


	void on_mouse_down(int x, int y, int button, unsigned int state) {
		switch(button) {
			case Button1: perform_action((state & ControlMask) ? left_ctrl_click_action : left_click_action); break;
			case Button2: perform_action(middle_click_action); break;
			case Button3: perform_action((state & ControlMask) ? right_ctrl_click_action : right_click_action); break;
			case Button4: perform_action(wheel_up_action); break;
			case Button5: perform_action(wheel_down_action); break;
		}
	}


	void on_double_click(int x, int y, int button, unsigned int state) {
		switch(button) {
			case Button1: perform_action(left_double_click_action); break;
			case Button2: perform_action(middle_double_click_action); break;
			case Button3: perform_action(right_double_click_action); break;
		}
	}


	void on_mouse_up(int x, int y, int button, unsigned int state) {
	}


	void on_mouse_move(int x, int y, unsigned int state) {
	}


	void on_mouse_enter(int x, int y, unsigned int state) {
		if(state & (ShiftMask | Button1Mask))
			XRaiseWindow(X::display, task->window);
		hovered = true;
		panel->invalidate();
	}


	void on_mouse_leave(unsigned int state) {
		hovered = false;
		panel->invalidate();
	}


	void perform_action(TaskbarAction action) {
		switch(action) {
			case ACTION_NONE:
				break;

			case ACTION_ACTIVATE_ICONIFY:
				if(task->iconified) {
					task->uniconify();
					task->activate();
				}
				else if(task->active)
					task->iconify();
				else
					task->activate();
				break;

			case ACTION_ACTIVATE:
				if(task->iconified)
					task->uniconify();
				task->activate();
				break;

			case ACTION_ICONIFY:
				if(!task->iconified)
					task->iconify();
				break;

			case ACTION_MAXIMIZE:
				if(task->iconified)
					task->uniconify();
				task->maximize();
				break;

			case ACTION_MAXIMIZE_LEFT:
				if(task->iconified)
					task->uniconify();
				task->maximize_left();
				break;

			case ACTION_MAXIMIZE_RIGHT:
				if(task->iconified)
					task->uniconify();
				task->maximize_right();
				break;

			case ACTION_SHOW:
				task->show();
				break;

			case ACTION_HIDE:
				task->hide();
				break;

			case ACTION_ROLL:
				task->roll();
				break;

			case ACTION_ROLLUP:
				task->roll_up();
				break;

			case ACTION_ROLLDOWN:
				task->roll_down();
				break;

			case ACTION_CLOSE:
				task->close();
				break;
		}
	}


	void paint_background(Surface & surface) {
		Color fill_color;
		if(task->attention)
			fill_color = hovered ? config.get_color("attention_hover_background") : config.get_color("attention_background");
		else if(task->active)
			fill_color = hovered ? config.get_color("active_hover_background") : config.get_color("active_background");
		else if(task->iconified)
			fill_color = hovered ? config.get_color("iconified_hover_background") : config.get_color("iconified_background");
		else
			fill_color = hovered ? config.get_color("normal_hover_background") : config.get_color("normal_background");
		surface.fill_rect(fill_color, x, y, width, height);

		if(task->icon != NULL) {
			Pixels resized_icon = task->icon->resize(icon_size, icon_size);
			surface.draw(resized_icon, 0, 0, x + 4, y + (height - resized_icon.height) / 2, resized_icon.width, resized_icon.height);
		}

		surface.vertical_gradient(border_color, x, 0, border_width, height);
		if(last_button)
			surface.vertical_gradient(border_color, x + width - border_width, 0, border_width, height);
	}


	void paint(XftDraw * xft_draw) {
		XftFont * font;
		XftColor color;
		if(task->attention) {
			color.color = (hovered ? config.get_color("attention_hover_color") : config.get_color("attention_color"));
			font = attention_font;
		}
		else if(task->active) {
			color.color = (hovered ? config.get_color("active_hover_color") : config.get_color("active_color"));
			font = active_font;
		}
		else if(task->iconified) {
			color.color = (hovered ? config.get_color("iconified_hover_color") : config.get_color("iconified_color"));
			font = iconified_font;
		}
		else {
			color.color = (hovered ? config.get_color("normal_hover_color") : config.get_color("normal_color"));
			font = normal_font;
		}

		if(task->icon == NULL) {
			std::string title = XftTextMaxWidth(xft_draw, normal_font, task->title, "...", width - 10);
			XGlyphInfo extents;
			XftTextExtentsUtf8(X::display, font, (unsigned char *)title.c_str(), title.length(), &extents);
			XftDrawStringUtf8(xft_draw, &color, font, x + 5 + (width - extents.width) / 2, y + 15, (unsigned char *)title.c_str(), title.length());
		}
		else {
			std::string title = XftTextMaxWidth(xft_draw, normal_font, task->title, "...", width - icon_size - 14);
			XftDrawStringUtf8(xft_draw, &color, font, x + icon_size + 9, y + 15, (unsigned char *)title.c_str(), title.length());
		}
	}
};


class Taskbar : public Widget, TaskListener, TasksListener {
public:
	Taskbar(Panel * panel, const Configuration & config) : Widget(panel), config(config), hovered_task(NULL) {
		desired_width = -1;
		screen = X::get_screen(Config::get_int("screen"));
		Tasks::add_listener(this);
		xdesktop = X::get_current_desktop(screen.xscreen);
	}


	TaskbarTask * get_task_at(int x, int y) {
		for(std::map<Task *, TaskbarTask *>::iterator iter = tasks.begin(); iter != tasks.end(); iter++)
			if((*iter).second->x <= x && (*iter).second->x + (int)(*iter).second->width > x && (*iter).second->y <= y && (*iter).second->y + (int)(*iter).second->height > y)
				return (*iter).second;
		return NULL;
	}


	void on_task_desktop_changed(Task * task) {
		pack();
		panel->invalidate();
	}


	void on_task_screen_changed(Task * task) {
		pack();
		panel->invalidate();
	}


	void on_task_activated() {
		panel->invalidate();
	}


	void on_task_created(Task * task) {
		task->add_listener(this);
		tasks[task] = new TaskbarTask(task, panel, config);
		pack();
		panel->invalidate();
	}


	void on_task_destroyed(Task * task) {
		tasks.erase(task);
		pack();
		panel->invalidate();
	}


	void pack() {
		int visible_tasks = 0;
		for(std::map<Task *, TaskbarTask *>::iterator iter = tasks.begin(); iter != tasks.end(); iter++)
			if(!(*iter).second->task->skip_pager && (*iter).second->task->screen == screen.id && (*iter).second->task->desktop == xdesktop)
				visible_tasks++;

		int x = 0;
		TaskbarTask * last_visible_task = NULL;
		for(std::map<Task *, TaskbarTask *>::iterator iter = tasks.begin(); iter != tasks.end(); iter++) {
			if(!(*iter).second->task->skip_pager && (*iter).second->task->screen == screen.id && (*iter).second->task->desktop == xdesktop) {
				(*iter).second->x = this->x + x;
				(*iter).second->y = this->y;
				(*iter).second->width = (this->width - x) / visible_tasks;
				(*iter).second->height = this->height;
				x += (*iter).second->width;
				visible_tasks--;
				last_visible_task = (*iter).second;
			}
			else {
				(*iter).second->x = 0;
				(*iter).second->y = 0;
				(*iter).second->width = 0;
				(*iter).second->height = 0;
			}
			(*iter).second->last_button = false;
		}

		if(last_visible_task != NULL)
			last_visible_task->last_button = true;
	}


	void paint_background(Surface & surface) {
		for(std::map<Task *, TaskbarTask *>::iterator iter = tasks.begin(); iter != tasks.end(); iter++)
			if((*iter).second->width > 0)
				(*iter).second->paint_background(surface);
	}


	void paint(XftDraw * xft_draw) {
		for(std::map<Task *, TaskbarTask *>::iterator iter = tasks.begin(); iter != tasks.end(); iter++)
			if((*iter).second->width > 0)
				(*iter).second->paint(xft_draw);
	}


	void on_mouse_down(int x, int y, int button, unsigned int state) {
		TaskbarTask * task = get_task_at(x, y);
		if(task != NULL)
			task->on_mouse_down(x, y, button, state);
	}


	void on_double_click(int x, int y, int button, unsigned int state) {
		TaskbarTask * task = get_task_at(x, y);
		if(task != NULL)
			task->on_double_click(x, y, button, state);
	}


	void on_mouse_up(int x, int y, int button, unsigned int state) {
		TaskbarTask * task = get_task_at(x, y);
		if(task != NULL)
			task->on_mouse_up(x, y, button, state);
	}


	void on_mouse_move(int x, int y, unsigned int state) {
		TaskbarTask * old_hover = hovered_task;
		hovered_task = get_task_at(x, y);
		if(old_hover != hovered_task && old_hover != NULL)
			old_hover->on_mouse_leave(state);
		if(old_hover != hovered_task && hovered_task != NULL)
			hovered_task->on_mouse_enter(x, y, state);
		if(old_hover == hovered_task && hovered_task != NULL)
			hovered_task->on_mouse_move(x, y, state);
	}


	void on_mouse_enter(int x, int y, unsigned int state) {
		hovered_task = get_task_at(x, y);
		if(hovered_task != NULL)
			hovered_task->on_mouse_enter(x, y, state);
	}


	void on_mouse_leave(unsigned int state) {
		if(hovered_task != NULL) {
			hovered_task->on_mouse_leave(state);
			hovered_task = NULL;
		}
	}


	void on_event(XEvent * ev) {
		if(ev->type == PropertyNotify && X::is_root_window(ev->xproperty.window)) {
			// current desktop changed
			if(ev->xproperty.atom == X::_NET_CURRENT_DESKTOP) {
				xdesktop = X::get_current_desktop(screen.xscreen);
				pack();
				panel->invalidate();
			}
		}
	}


private:
	Configuration config;
	std::map<Task *, TaskbarTask *> tasks;
	TaskbarTask * hovered_task;
	X::XScreen screen;
	int xdesktop;
};


#endif
