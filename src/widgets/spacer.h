#ifndef INCLUDE_WIDGETS_SPACER_H
#define INCLUDE_WIDGETS_SPACER_H


#include "../panel.h"


class Spacer : public Widget {
public:
	Spacer(Panel * panel, int width) : Widget(panel) {
		desired_width = width;
	}


	Spacer(Panel * panel, const Configuration & config) : Widget(panel) {
		desired_width = config.get_int("width", 10);
	}
};


#endif
