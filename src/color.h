#ifndef INCLUDE_COLOR_H
#define INCLUDE_COLOR_H


#include <stdint.h> // uint*_t
#include <X11/Xft/Xft.h> // XRenderColor


struct Color {
	typedef uint8_t Channel;


	union {
		struct {
			Channel b, g, r, a;
		};
		Channel channels[4];
		uint32_t argb;
	};


	Color() : argb(0) {
	}


	Color(uint32_t argb) : argb(argb) {
	}


	Color(Channel r, Channel g, Channel b, Channel a = 0) : b(b), g(g), r(r), a(a) {
	}


	operator uint32_t() const {
		return argb;
	}


	bool opaque() const {
		return (a == 0xFF);
	}


	Color over(const Color & c) const {
		if(a == 0x00)
			return c;
		if(a == 0xFF)
			return *this;
		Channel alpha = 255 - a;
		return Color((a * r + alpha * c.r) / 255, (a * g + alpha * c.g) / 255, (a * b + alpha * c.b) / 255, a + (alpha * c.a) / 255);
	}


	Color glow(float factor) const {
		return Color(std::min(255.0f, r * factor), std::min(255.0f, g * factor), std::min(255.0f, b * factor), a);
	}


	Color combine(const Color & c, float ratio = 0.5) const {
		return Color(ratio * c.r + (1 - ratio) * r, ratio * c.g + (1 - ratio) * g, ratio * c.b + (1 - ratio) * b, ratio * c.a + (1 - ratio) * a);

	}


	Color invert() const {
		return Color(255 - r, 255 - g, 255 - b, a);
	}


	Color luminance() const {
		Channel rgb = (30 * r + 59 * g + 11 * b) / 100;
		return Color(rgb, rgb, rgb, a);
	}


	operator XRenderColor() {
		return (XRenderColor){ (uint16_t)(((uint16_t)r << 8) | r), (uint16_t)((uint16_t)(g << 8) | g), (uint16_t)(((uint16_t)b << 8) | b), 
(uint16_t)(((uint16_t)a << 8) 
| a) };
	}
};


#endif
