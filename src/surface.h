#ifndef INCLUDE_SURFACE_H
#define INCLUDE_SURFACE_H


#include <X11/Xutil.h>
#include <math.h>

#include "pixels.h"


static void adjust_rect(size_t max_width, size_t max_height, int * x, int * y, size_t * width, size_t * height) {
	if(*width < 0) {
		*x += *width;
		*width = -*width;
	}
	if(height < 0) {
		*y += *height;
		*height = -*height;
	}
	if(*x < 0) {
		*width += *x;
		*x = 0;
	}
	if(*y < 0) {
		*height += *y;
		*y = 0;
	}
	if(*x > (int)max_width || *y > (int)max_height)
		*x = *y = *width = *height = 0;
	else {
		if(*width > max_width)
			*width = max_width;
		if(*height > max_height)
			*height = max_height;
	}
}


struct Surface {
	size_t width, height;
	XImage * ximage;

	Surface(size_t width, size_t height) : width(width), height(height) {
		char * pixels = (char *)malloc(width * height * sizeof(uint32_t));
		ximage = XCreateImage(X::display, XDefaultVisual(X::display, XDefaultScreen(X::display)), XDefaultDepth(X::display, XDefaultScreen(X::display)), ZPixmap, 0, pixels, width, height, 32, width * 4);
	}


	Surface(XImage * ximage) : width(ximage->width), height(ximage->height), ximage(ximage) {
	}


	~Surface() {
		if(ximage != NULL)
			XDestroyImage(ximage);
	}


	void draw(const Pixels & src, int src_x, int src_y, int dest_x, int dest_y, size_t width, size_t height) {
		adjust_rect(this->width, this->height, &dest_x, &dest_y, &width, &height);
		adjust_rect(src.width, src.height, &src_x, &src_y, &width, &height);

		if(src.opaque) {
			for(size_t y = 0; y < height; y++)
				for(size_t x = 0; x < width; x++)
					XPutPixel(ximage, dest_x + x, dest_y + y, src.data[src.width * (src_y + y) + (src_x + x)]);
		}
		else {
			for(size_t y = 0; y < height; y++)
				for(size_t x = 0; x < width; x++)
					XPutPixel(ximage, dest_x + x, dest_y + y, src.data[src.width * (src_y + y) + (src_x + x)].over(XGetPixel(ximage, dest_x + x, dest_y + y)));
		}
	}


	void draw(Surface & src, int src_x, int src_y, int dest_x, int dest_y, size_t width, size_t height) {
		draw(src.ximage, src_x, src_y, dest_x, dest_y, width, height);
	}


	void draw(XImage * src, int src_x, int src_y, int dest_x, int dest_y, size_t width, size_t height) {
		adjust_rect(this->width, this->height, &dest_x, &dest_y, &width, &height);
		adjust_rect(src->width, src->height, &src_x, &src_y, &width, &height);

		for(size_t y = 0; y < height; y++)
			for(size_t x = 0; x < width; x++)
				XPutPixel(ximage, dest_x + x, dest_y + y, XGetPixel(src, src_x + x, src_y + y));
	}


	void fill_rect(Color color, int x, int y, size_t width, size_t height) {
		adjust_rect(this->width, this->height, &x, &y, &width, &height);

		if(color.a == 0xFF) {
			for(size_t j = 0; j < height; j++)
				for(size_t i = 0; i < width; i++)
					XPutPixel(ximage, x + i, y + j, color);
		}
		else if(color.a != 0x00) {
			for(size_t j = 0; j < height; j++)
				for(size_t i = 0; i < width; i++)
					XPutPixel(ximage, x + i, y + j, color.over(XGetPixel(ximage, x + i, y + j)));
		}
	}


	void draw_to(Drawable drawable, GC gc, int src_x, int src_y, int dest_x, int dest_y, size_t width, size_t height) {
		XPutImage(X::display, drawable, gc, ximage, src_x, src_y, dest_x, dest_y, width, height);
	}


	Color get_pixel_repeat(int x, int y) {
		x = (x < 0) ? 0 : ((x >= (int)width) ? (int)width - 1 : x);
		y = (y < 0) ? 0 : ((y >= (int)height) ? (int)height - 1 : y);
		return XGetPixel(ximage, x, y);
	}


	Color fold(int left, int top, double * matrix, size_t width, size_t height) {
		double r = 0, g = 0, b = 0, a = 0;
		for(int y = top; y < top + (int)height; y++) {
			for(int x = left; x < left + (int)width; x++) {
				Color c = get_pixel_repeat(x, y);
				r += matrix[0] * c.r;
				g += matrix[0] * c.g;
				b += matrix[0] * c.b;
				a += matrix[0] * c.a;
				matrix++;
			}
		}
		return Color(r, g, b, a);
	}


	void filter_blur(double radius) {
		int offset = abs((int)ceil(radius)) * 3;
		size_t size = 2 * offset + 1;

		// calculate gaussian distribution
		double weight = 0;
		double * weights = new double[size];
		for(size_t i = 0; i < size; i++)
			weight += weights[i] = 0.159154943 / (radius * radius) * exp(-((int)i - offset) * ((int)i - offset) / (2 * radius*radius));

		// normalize distribution to sum == 1.0
		for(size_t i = 0; i < size; i++)
			weights[i] /= weight;

		Pixels buffer(width, height);

		for(size_t y = 0; y < height; y++)
			for(size_t x = 0; x < width; x++)
				buffer(x, y) = fold(x - offset, y, weights, size, 1);

		for(size_t y = 0; y < height; y++)
			for(size_t x = 0; x < width; x++)
				XPutPixel(ximage, x, y, buffer.fold(x, y - offset, weights, 1, size));

		delete[] weights;
	}


	void filter_invert() {
		for(size_t y = 0; y < height; y++)
			for(size_t x = 0; x < width; x++)
				XPutPixel(ximage, x, y, Color(XGetPixel(ximage, x, y)).invert());
	}


	void filter_luminance() {
		for(size_t y = 0; y < height; y++)
			for(size_t x = 0; x < width; x++)
				XPutPixel(ximage, x, y, Color(XGetPixel(ximage, x, y)).luminance());
	}


	void vertical_gradient(std::vector<Color> colors, int left, int top, int width, int height) {
		if(colors.empty())
			return;

		for(int y = 0; y < height; y++) {
			float ratio = ((float)y + (float)y / (float)height) / (float)height * (float)((int)colors.size() - 1);
			size_t index = (size_t)ratio;
			ratio -= index;
			Color c = (index + 1 >= colors.size()) ? colors.back() : colors[index].combine(colors[index + 1], ratio);
			for(int x = 0; x < width; x++)
				XPutPixel(ximage, x + left, y + top, c.over(Color(XGetPixel(ximage, x + left, y + top))));
		}
	}


	void vertical_gradient(Color top, Color bottom) {
		for(size_t y = 0; y < height; y++) {
			Color c = top.combine(bottom, ((float)y + (float)y / (float)height) / (float)height);
			for(size_t x = 0; x < width; x++)
				XPutPixel(ximage, x, y, c.over(Color(XGetPixel(ximage, x, y))));
		}
	}


	void vertical_gradient(Color top, Color bottom, int left, int width) {
		for(size_t y = 0; y < height; y++) {
			Color c = top.combine(bottom, ((float)y + (float)y / (float)height) / (float)height);
			for(int x = 0; x < width; x++)
				XPutPixel(ximage, x + left, y, c.over(Color(XGetPixel(ximage, x + left, y))));
		}
	}
};


#endif
