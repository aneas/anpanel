#ifndef INCLUDE_TASKS_H
#define INCLUDE_TASKS_H


#include <string>
#include <list>
#include <map>
#include <set>

#include <unistd.h>

#include "pixels.h"
#include "x.h"


class Task;


class TaskListener {
public:
	virtual void on_task_title_changed(Task * task) { }
	virtual void on_task_desktop_changed(Task * task) { }
	virtual void on_task_icon_changed(Task * task) { }
	virtual void on_task_position_changed(Task * task) { }
	virtual void on_task_state_changed(Task * task) { }
	virtual void on_task_screen_changed(Task * task) { }
};


class Task {
public:
	int xscreen;
	Window window;

	std::string title;
	Pixels * icon;

	int desktop;
	size_t screen;
	int x, y;
	size_t width, height;
	bool iconified;
	bool shaded;
	bool skip_pager;
	bool active;
	bool attention;

	bool marked; // used by class Tasks
	std::list<TaskListener *> listeners;


public:
	Task(int xscreen, Window window) : xscreen(xscreen), window(window), icon(NULL), screen((size_t)-1) {
		update();
	}


	void add_listener(TaskListener * listener) {
		listeners.push_back(listener);
	}


	void remove_listener(TaskListener * listener) {
		listeners.remove(listener);
	}


	void update_title() {
		title = X::get_window_title(window);

		for(std::list<TaskListener *>::iterator iter = listeners.begin(); iter != listeners.end(); iter++)
			(*iter)->on_task_title_changed(this);
	}


	void update_desktop() {
		desktop = X::get_window_desktop(window);

		for(std::list<TaskListener *>::iterator iter = listeners.begin(); iter != listeners.end(); iter++)
			(*iter)->on_task_desktop_changed(this);
	}


	void update_icon() {
		if(icon != NULL)
			delete icon;
		icon = X::get_window_icon(window);

		for(std::list<TaskListener *>::iterator iter = listeners.begin(); iter != listeners.end(); iter++)
			(*iter)->on_task_icon_changed(this);
	}


	void update_position() {
		Window unused;
		XTranslateCoordinates(X::display, window, XRootWindow(X::display, xscreen), 0, 0, &x, &y, &unused);

		XWindowAttributes attributes;
		XGetWindowAttributes(X::display, window, &attributes);
		width = attributes.width;
		height = attributes.height;

		for(std::list<TaskListener *>::iterator iter = listeners.begin(); iter != listeners.end(); iter++)
			(*iter)->on_task_position_changed(this);

		update_screen();
	}


	void update_screen() {
		size_t old_screen = screen;
		screen = X::get_screen_at(x + width / 2, y + height / 2).id;
		if(screen != old_screen)
			for(std::list<TaskListener *>::iterator iter = listeners.begin(); iter != listeners.end(); iter++)
				(*iter)->on_task_screen_changed(this);
	}


	void update_status() {
		bool old_iconified = iconified;
		bool old_shaded = shaded;
		bool old_skip_pager = skip_pager;
		bool old_attention = attention;

		XWindowAttributes attributes;
		XGetWindowAttributes(X::display, window, &attributes);
		iconified = (attributes.map_state != IsViewable);
		shaded = false;
		skip_pager = false;
		attention = false;

		Atom * a = (Atom *)X::get_window_property(window, X::_NET_WM_WINDOW_TYPE, XA_ATOM, NULL);
		if(a != NULL) {
			if(*a == X::_NET_WM_WINDOW_TYPE_DOCK || *a == X::_NET_WM_WINDOW_TYPE_DESKTOP)
				skip_pager = true;
			XFree(a);
		}

		int count;
		a = (Atom *)X::get_window_property(window, X::_NET_WM_STATE, XA_ATOM, &count);
		if(a != NULL) {
			for(int i = 0; i < count; i++) {
				if(a[i] == X::_NET_WM_STATE_SKIP_PAGER || a[i] == X::_NET_WM_STATE_SKIP_TASKBAR)
					skip_pager = true;
				if(a[i] == X::_NET_WM_STATE_DEMANDS_ATTENTION)
					attention = true;
				if(a[i] == X::_NET_WM_STATE_SHADED)
					shaded = true;
			}
			XFree(a);
		}

		if(X::is_window_urgent(window))
			attention = true;

		if(old_iconified != iconified || old_shaded != shaded || old_skip_pager != skip_pager || old_attention != attention)
			for(std::list<TaskListener *>::iterator iter = listeners.begin(); iter != listeners.end(); iter++)
				(*iter)->on_task_state_changed(this);
	}


	void update() {
		update_title();
		update_desktop();
		update_icon();
		update_position();
		update_status();
	}


	bool is_on_screen(const X::XScreen & screen) {
		if(skip_pager)
			return false;
		if(desktop != -1 && desktop != X::get_current_desktop(screen.xscreen))
			return false;
		return (x + (int)width > screen.x && x < screen.x + (int)screen.width && y + (int)height > screen.y && y < screen.y + (int)screen.height);
	}


	void iconify() {
		X::iconify_window(xscreen, window);
	}


	void uniconify() {
		X::uniconify_window(window);
	}


	void activate() {
		X::activate_window(xscreen, window);
	}


	void maximize() {
		X::send_event(xscreen, window, X::_NET_WM_STATE, X::_NET_WM_STATE_TOGGLE, X::_NET_WM_STATE_MAXIMIZED_VERT);
		X::send_event(xscreen, window, X::_NET_WM_STATE, X::_NET_WM_STATE_TOGGLE, X::_NET_WM_STATE_MAXIMIZED_HORZ);
	}


	void maximize_left() {
		X::send_event(xscreen, window, X::_NET_WM_STATE, X::_NET_WM_STATE_REMOVE, X::_NET_WM_STATE_MAXIMIZED_VERT);
		X::send_event(xscreen, window, X::_NET_WM_STATE, X::_NET_WM_STATE_REMOVE, X::_NET_WM_STATE_MAXIMIZED_HORZ);
		if(Config::get("position") == "top")
			X::XMoveResizeWindowDecoration(xscreen, window, X::screens[screen].x, X::screens[screen].y + Config::get_int("height"), X::screens[screen].width / 2, X::screens[screen].height - Config::get_int("height"));
		else
			X::XMoveResizeWindowDecoration(xscreen, window, X::screens[screen].x, X::screens[screen].y, X::screens[screen].width / 2, X::screens[screen].height - Config::get_int("height"));
		XRaiseWindow(X::display, window);
	}


	void maximize_right() {
		X::send_event(xscreen, window, X::_NET_WM_STATE, X::_NET_WM_STATE_REMOVE, X::_NET_WM_STATE_MAXIMIZED_VERT);
		X::send_event(xscreen, window, X::_NET_WM_STATE, X::_NET_WM_STATE_REMOVE, X::_NET_WM_STATE_MAXIMIZED_HORZ);
		if(Config::get("position") == "top")
			X::XMoveResizeWindowDecoration(xscreen, window, X::screens[screen].x + X::screens[screen].width / 2, X::screens[screen].y + Config::get_int("height"), X::screens[screen].width - X::screens[screen].width / 2, X::screens[screen].height - Config::get_int("height"));
		else
			X::XMoveResizeWindowDecoration(xscreen, window, X::screens[screen].x + X::screens[screen].width / 2, X::screens[screen].y, X::screens[screen].width - X::screens[screen].width / 2, X::screens[screen].height - Config::get_int("height"));
		XRaiseWindow(X::display, window);
	}


	void show() {
		XRaiseWindow(X::display, window);
	}


	void hide() {
		XLowerWindow(X::display, window);
	}


	void roll() {
		X::send_event(xscreen, window, X::_NET_WM_STATE, X::_NET_WM_STATE_TOGGLE, X::_NET_WM_STATE_SHADED);
	}


	void roll_up() {
		X::send_event(xscreen, window, X::_NET_WM_STATE, X::_NET_WM_STATE_ADD, X::_NET_WM_STATE_SHADED);
	}


	void roll_down() {
		X::send_event(xscreen, window, X::_NET_WM_STATE, X::_NET_WM_STATE_REMOVE, X::_NET_WM_STATE_SHADED);
	}


	void close() {
		X::send_event(xscreen, window, X::_NET_CLOSE_WINDOW, 0, 2);
	}
};


class TasksListener {
public:
	virtual void on_task_activated() { }
	virtual void on_task_created(Task * task) { }
	virtual void on_task_destroyed(Task * task) { }
};


namespace Tasks {
	std::map<Window, Task *> tasks;
	std::list<TasksListener *> listeners;


	void add_listener(TasksListener * listener) {
		listeners.push_back(listener);
	}


	void remove_listener(TasksListener * listener) {
		listeners.remove(listener);
	}


	Task * get_task(Window window) {
		std::map<Window, Task *>::iterator iter = tasks.find(window);
		if(iter != tasks.end())
			return (*iter).second;
		return NULL;
	}


	void update_all_tasks() {
		for(std::map<Window, Task *>::iterator iter = tasks.begin(); iter != tasks.end(); iter++)
			(*iter).second->marked = false;

		int screen_count = XScreenCount(X::display);
		for(int xscreen = 0; xscreen < screen_count; xscreen++) {
			int window_count;
			Window * windows = (Window *)X::get_screen_property(xscreen, X::_NET_CLIENT_LIST, XA_WINDOW, &window_count);

			for(int i = 0; i < window_count; i++) {
				std::map<Window, Task *>::iterator iter = tasks.find(windows[i]);
				if(iter != tasks.end())
					(*iter).second->marked = true;
				else {
					X::select_input(X::display, windows[i], PropertyChangeMask | StructureNotifyMask);
					Task * task = new Task(xscreen, windows[i]);
					task->marked = true;
					tasks[windows[i]] = task;
					for(std::list<TasksListener *>::iterator iter = listeners.begin(); iter != listeners.end(); iter++)
						(*iter)->on_task_created(task);
				}
			}

			XFree(windows);
		}

		for(std::map<Window, Task *>::iterator iter = tasks.begin(); iter != tasks.end(); iter++) {
			if(!(*iter).second->marked) {
				for(std::list<TasksListener *>::iterator iter2 = listeners.begin(); iter2 != listeners.end(); iter2++)
					(*iter2)->on_task_destroyed((*iter).second);
				delete (*iter).second;
				tasks.erase(iter);
			}
		}
	}


	void update_active_task() {
		Window active_window = X::get_active_window(0);
		for(std::map<Window, Task *>::iterator iter = tasks.begin(); iter != tasks.end(); iter++)
			(*iter).second->active = ((*iter).second->window == active_window);
		for(std::list<TasksListener *>::iterator iter = listeners.begin(); iter != listeners.end(); iter++)
			(*iter)->on_task_activated();
	}


	void on_event(XEvent * ev) {
		if(ev->type == PropertyNotify) {
			if(X::is_root_window(ev->xproperty.window)) {
				// window created or destroyed
				if(ev->xproperty.atom == X::_NET_CLIENT_LIST)
					update_all_tasks();
				// active window changed
				else if(ev->xproperty.atom == X::_NET_ACTIVE_WINDOW)
					update_active_task();
			}
			else {
				// application desktop changed
				if(ev->xproperty.atom == X::_NET_WM_DESKTOP) {
					Task * task = get_task(ev->xproperty.window);
					if(task != NULL)
						task->update_desktop();
				}
				// application title changed
				else if(ev->xproperty.atom == X::_NET_WM_VISIBLE_NAME) {
					Task * task = get_task(ev->xproperty.window);
					if(task != NULL)
						task->update_title();
				}
				// application icon changed
				else if(ev->xproperty.atom == X::_NET_WM_ICON) {
					Task * task = get_task(ev->xproperty.window);
					if(task != NULL)
						task->update_icon();
				}
				// application icon changed
				else if(ev->xproperty.atom == X::WM_HINTS) {
					Task * task = get_task(ev->xproperty.window);
					if(task != NULL)
						task->update_status();
				}
			}
		}
		else if(ev->type == ConfigureNotify) {
			Task * task = get_task(ev->xconfigure.window);
			if(task != NULL) {
				task->update_position();
				task->update_status();
			}
		}
		else if(ev->type == DestroyNotify) {
			std::map<Window, Task *>::iterator iter = tasks.find(ev->xdestroywindow.window);
			if(iter != tasks.end()) {
				for(std::list<TasksListener *>::iterator iter2 = listeners.begin(); iter2 != listeners.end(); iter2++)
					(*iter2)->on_task_destroyed((*iter).second);
				delete (*iter).second;
				tasks.erase(iter);
			}
		}
	}
};


void launch(const std::string & command) {
	if(command.empty())
		return;

	/* create independent child process */
	pid_t pid = fork();
	if(pid < 0) {
		fprintf(stderr, "Could not fork.\n");
		return;
	}
	if(pid == 0) {
		/* change the file mode mask */
		umask(0);

		/* create a new session id */
		pid_t sid = setsid();
		if(sid < 0)
			exit(EXIT_FAILURE);

		/* change the current working directory */
		if((chdir("/")) < 0)
			exit(EXIT_FAILURE);

		/* close out the standard file descriptors */
		close(STDIN_FILENO);
		close(STDOUT_FILENO);
		close(STDERR_FILENO);

		execl("/bin/sh", "/bin/sh", "-c", command.c_str(), NULL);
		exit(EXIT_SUCCESS);
	}
}


#endif
