#ifndef INCLUDE_PANEL_H
#define INCLUDE_PANEL_H


#include <vector>
#include <X11/Xlib.h>

#include "x.h"
#include "surface.h"
#include "config.h"


const unsigned long DOUBLE_CLICK_TIME = 200;


class Panel;


class Widget {
public:
	Panel * panel;
	int x, y;
	size_t width, height;
	int desired_width;

	Widget(Panel * panel) : panel(panel) { }
	virtual ~Widget() { }
	virtual void pack() { }
	virtual void paint_background(Surface & surface) { }
	virtual void paint(XftDraw * xft_draw) { }
	virtual void on_mouse_down(int x, int y, int button, unsigned int state) { }
	virtual void on_double_click(int x, int y, int button, unsigned int state) { }
	virtual void on_mouse_up(int x, int y, int button, unsigned int state) { }
	virtual void on_mouse_move(int x, int y, unsigned int state) { }
	virtual void on_mouse_enter(int x, int y, unsigned int state) { }
	virtual void on_mouse_leave(unsigned int state) { }
	virtual void on_timer() { }
	virtual void on_event(XEvent * event) { }
};


class Panel {
public:
	X::XScreen screen;
	int x, y;
	size_t width, height;
	Window window;
	GC gc;
	XftDraw * xft_draw;
	Surface * background;

	std::vector<Widget *> widgets;
	Widget * mouse_widget;

	Pixels * corner_left, * corner_right;

	// used for double click
	Time last_left_click, last_right_click, last_middle_click;

public:
	Panel() : mouse_widget(NULL), last_left_click(0), last_right_click(0), last_middle_click(0) {
		screen = X::get_screen(Config::get_int("screen"));

		x = screen.x;
		y = (Config::get("position") == "top") ? screen.y : (screen.y + screen.height - Config::get_int("height"));
		width = screen.width;
		height = Config::get_int("height");

		window = XCreateSimpleWindow(X::display, screen.root, 0, 0, width, height, 0, 0, 0);
		XWMHints * hints = XAllocWMHints();
		hints->flags = InputHint | StateHint;
		hints->input = 0;
		hints->initial_state = NormalState;
		XSetWMHints(X::display, window, hints);
		XSizeHints * size_hints = XAllocSizeHints();
		size_hints->flags = PPosition | PMinSize | PMaxSize;
		size_hints->min_width = size_hints->max_width = width;
		size_hints->min_height = size_hints->max_height = height;
		XSetWMNormalHints(X::display, window, size_hints);
		X::set_window_decoration(window, 0);
		X::set_window_desktop(window, -1);
		X::set_window_type(window, X::_NET_WM_WINDOW_TYPE_DOCK);
		X::select_input(X::display, window, ExposureMask | StructureNotifyMask | ButtonPressMask | ButtonReleaseMask | PointerMotionMask | EnterWindowMask | LeaveWindowMask);
		XMapWindow(X::display, window);
		XMoveResizeWindow(X::display, window, x, y, width, height);

		if(Config::get("position") == "top")
			X::reserve_screen_margin(window, 0, 0, Config::get_int("height"), 0);
		else
			X::reserve_screen_margin(window, 0, 0, 0, Config::get_int("height"));

		gc = XCreateGC(X::display, window, 0, NULL);
		xft_draw = XftDrawCreate(X::display, window, XDefaultVisual(X::display, screen.xscreen), XDefaultColormap(X::display, screen.xscreen));

		background = NULL;
		update_background();


		int corner_size = Config::get_int("corner_size");
		corner_left = new Pixels(corner_size, corner_size);
		corner_right = new Pixels(corner_size, corner_size);
		for(int y = 0; y < corner_size; y++) {
			for(int x = 0; x < corner_size; x++) {
				float distance = sqrt(x * x + y * y);
				Color c;
				if(distance >= corner_size)
					c = Color(0, 0, 0, 255);
				else if(distance >= corner_size - 1)
					c = Color(0, 0, 0, 255 * (distance - corner_size - 1));
				(*corner_left)(corner_size - 1 - x, y) = c;
				(*corner_right)(x, y) = c;
			}
		}
		if(Config::get("position") == "top") {
			corner_left->flip_vertical_inplace();
			corner_right->flip_vertical_inplace();
		}

		pack();
	}


	~Panel() {
		for(size_t i = 0; i < widgets.size(); i++)
			delete widgets[i];
	}


	void invalidate() {
		paint();
	}


	void pack() {
		int fluid_width = width;
		size_t fluid_widgets = 0;
		for(size_t i = 0; i < widgets.size(); i++) {
			if(widgets[i]->desired_width >= 0)
				fluid_width -= widgets[i]->desired_width;
			else
				fluid_widgets++;
		}

		int x = 0;
		for(size_t i = 0; i < widgets.size(); i++) {
			widgets[i]->x = x;
			widgets[i]->y = 0;
			if(widgets[i]->desired_width >= 0)
				widgets[i]->width = widgets[i]->desired_width;
			else {
				widgets[i]->width = fluid_width / fluid_widgets;
				fluid_width -= widgets[i]->width;
				fluid_widgets--;
			}
			widgets[i]->height = height;
			x += widgets[i]->width;

			widgets[i]->pack();
		}

		invalidate();
	}


	void add(Widget * widget) {
		widgets.push_back(widget);
		pack();
	}


	void paint() {
		Surface surface(width, height);
		if(background != NULL)
			surface.draw(*background, 0, 0, 0, 0, width, height);

		for(size_t i = 0; i < widgets.size(); i++)
			widgets[i]->paint_background(surface);

		if(Config::get("position") == "top") {
			surface.draw(*corner_left, 0, 0, 0, 0, corner_left->width, corner_left->height);
			surface.draw(*corner_right, 0, 0, width - corner_right->width, 0, corner_right->width, corner_right->height);
		}
		else {
			surface.draw(*corner_left, 0, 0, 0, height - corner_left->height, corner_left->width, corner_left->height);
			surface.draw(*corner_right, 0, 0, width - corner_right->width, height - corner_right->height, corner_right->width, corner_right->height);
		}

		surface.draw_to(window, gc, 0, 0, 0, 0, width, height);

		for(size_t i = 0; i < widgets.size(); i++)
			widgets[i]->paint(xft_draw);
	}


	void update_background() {
		if(background != NULL) {
			delete background;
			background = NULL;
		}

		if(!Config::get_color("background_color").opaque()) {
			XImage * ximage = X::get_desktop_background(screen.xscreen, x, y, width, height);
			if(ximage != NULL) {
				background = new Surface(ximage);

				if(Config::get("background_filter") == "blur")
					background->filter_blur(Config::get_float("background_blur_radius"));
				else if(Config::get("background_filter") == "invert")
					background->filter_invert();
				else if(Config::get("background_filter") == "luminance")
					background->filter_luminance();
			}
		}

		if(background == NULL)
			background = new Surface(width, height);

		background->vertical_gradient(Config::get_gradient("background"), 0, 0, width, height);

		// set the window background, so that systray icons with their background set to CopyFromParent use this and appear transparent
		Pixmap bg = XCreatePixmap(X::display, window, width, height, XDefaultDepth(X::display, screen.xscreen));
		background->draw_to(bg, gc, 0, 0, 0, 0, width, height);
		XSetWindowBackgroundPixmap(X::display, window, bg);
		XFreePixmap(X::display, bg);
	}


	Widget * get_widget_at(int x, int y) {
		for(size_t i = 0; i < widgets.size(); i++)
			if(widgets[i]->x <= x && widgets[i]->x + (int)widgets[i]->width > x && widgets[i]->y <= y && widgets[i]->y + (int)widgets[i]->height > y)
				return widgets[i];
		return NULL;
	}


	void on_mouse_down(int x, int y, int button, unsigned int state) {
		Widget * widget = get_widget_at(x, y);
		if(widget != NULL)
			widget->on_mouse_down(x, y, button, state);
	}


	void on_double_click(int x, int y, int button, unsigned int state) {
		Widget * widget = get_widget_at(x, y);
		if(widget != NULL)
			widget->on_double_click(x, y, button, state);
	}


	void on_mouse_up(int x, int y, int button, unsigned int state) {
		Widget * widget = get_widget_at(x, y);
		if(widget != NULL)
			widget->on_mouse_up(x, y, button, state);
	}


	void on_mouse_move(int x, int y, unsigned int state) {
		Widget * old_mouse_widget = mouse_widget;
		mouse_widget = get_widget_at(x, y);
		if(old_mouse_widget != mouse_widget && old_mouse_widget != NULL)
			old_mouse_widget->on_mouse_leave(state);
		if(old_mouse_widget != mouse_widget && mouse_widget != NULL)
			mouse_widget->on_mouse_enter(x, y, state);
		if(old_mouse_widget == mouse_widget && mouse_widget != NULL)
			mouse_widget->on_mouse_move(x, y, state);
	}


	void on_mouse_enter(int x, int y, unsigned int state) {
		mouse_widget = get_widget_at(x, y);
		if(mouse_widget != NULL)
			mouse_widget->on_mouse_enter(x, y, state);
	}


	void on_mouse_leave(unsigned int state) {
		if(mouse_widget != NULL) {
			mouse_widget->on_mouse_leave(state);
			mouse_widget = NULL;
		}
	}


	void on_timer() {
		for(size_t i = 0; i < widgets.size(); i++)
			widgets[i]->on_timer();
	}


	void on_event(XEvent * ev) {
		if(ev->xany.window == window) {
			if(ev->type == Expose)
				invalidate();
			else if(ev->type == ButtonPress) {
				if(ev->xbutton.button == Button1 && ev->xbutton.time > last_left_click && ev->xbutton.time <= last_left_click + DOUBLE_CLICK_TIME)
					on_double_click(ev->xbutton.x, ev->xbutton.y, ev->xbutton.button, ev->xbutton.state);
				else if(ev->xbutton.button == Button2 && ev->xbutton.time > last_middle_click && ev->xbutton.time <= last_middle_click + DOUBLE_CLICK_TIME)
					on_double_click(ev->xbutton.x, ev->xbutton.y, ev->xbutton.button, ev->xbutton.state);
				else if(ev->xbutton.button == Button3 && ev->xbutton.time > last_right_click && ev->xbutton.time <= last_right_click + DOUBLE_CLICK_TIME)
					on_double_click(ev->xbutton.x, ev->xbutton.y, ev->xbutton.button, ev->xbutton.state);
				else
					on_mouse_down(ev->xbutton.x, ev->xbutton.y, ev->xbutton.button, ev->xbutton.state);

				if(ev->xbutton.button == Button1)
					last_left_click = ev->xbutton.time;
				else if(ev->xbutton.button == Button2)
					last_middle_click = ev->xbutton.time;
				else if(ev->xbutton.button == Button3)
					last_right_click = ev->xbutton.time;
			}
			else if(ev->type == ButtonRelease)
				on_mouse_up(ev->xbutton.x, ev->xbutton.y, ev->xbutton.button, ev->xbutton.state);
			else if(ev->type == MotionNotify)
				on_mouse_move(ev->xmotion.x, ev->xmotion.y, ev->xmotion.state);
			else if(ev->type == EnterNotify)
				on_mouse_enter(ev->xcrossing.x, ev->xcrossing.y, ev->xcrossing.state);
			else if(ev->type == LeaveNotify)
				on_mouse_leave(ev->xcrossing.state);
		}

		if(ev->type == PropertyNotify && X::is_root_window(ev->xproperty.window)) {
			// desktop background changed
			if(ev->xproperty.atom == X::_XROOTPMAP_ID) {
				update_background();
				invalidate();
			}
		}

		for(size_t i = 0; i < widgets.size(); i++)
			widgets[i]->on_event(ev);
	}
};


#endif
