#ifndef INCLUDE_CONFIG_H
#define INCLUDE_CONFIG_H


#include <map>
#include <vector>
#include <string>

#include "color.h"


struct ConfigValue {
	std::string s;
	bool b;
	int i;
	float f;
	Color c;
	std::vector<Color> g;


	ConfigValue() : s(""), b(false), i(0), f(0.0) {
	}


	ConfigValue(std::string s) : s(s) {
		b = (s == "true");
		i = atoi(s.c_str());
		f = atof(s.c_str());
		const char * str = s.c_str();
		c = parse_color(str);
		g = parse_gradient(s.c_str());
	}


private:
	static Color parse_color(const char *& str) {
		if(str[0] == '#')
			str++;

		int r, g, b, a;
		if(sscanf(str, "%02X%02X%02X%02X", &a, &r, &g, &b) == 4) {
			str += 8;
			return Color(r, g, b, a);
		}
		if(sscanf(str, "%02X%02X%02X", &r, &g, &b) == 3) {
			str += 6;
			return Color(r, g, b, 0xFF);
		}
		return Color(0xFF000000);
	}


	static std::vector<Color> parse_gradient(const char * str) {
		std::vector<Color> gradient;
		while(str[0] != '\0') {
			const char * end = str;
			Color c = parse_color(end);
			if(str == end)
				break;
			gradient.push_back(c);
			str = end + strspn(end, " \t");
		}
		return gradient;
	}
};


class Configuration {
public:
	void set(std::string key, ConfigValue value) {
		values[key] = value;
	}


	void set(std::string key, std::string value) {
		if(value[0] == '$') {
			std::map<std::string, ConfigValue>::const_iterator iter = values.find(value.substr(1));
			if(iter != values.end()) {
				values[key] = (*iter).second;
				return;
			}
		}

		values[key] = ConfigValue(value);
	}


	bool isset(std::string key) const {
		return (values.find(key) != values.end());
	}


	bool empty() const {
		return values.empty();
	}


	std::string get(std::string key, std::string default_value = "") const {
		std::map<std::string, ConfigValue>::const_iterator iter = values.find(key);
		if(iter != values.end())
			return (*iter).second.s;
		return default_value;
	}


	bool get_bool(std::string key, bool default_value = false) const {
		std::map<std::string, ConfigValue>::const_iterator iter = values.find(key);
		if(iter != values.end())
			return (*iter).second.b;
		return default_value;
	}


	int get_int(std::string key, int default_value = 0) const {
		std::map<std::string, ConfigValue>::const_iterator iter = values.find(key);
		if(iter != values.end())
			return (*iter).second.i;
		return default_value;
	}


	float get_float(std::string key, float default_value = 0.0) const {
		std::map<std::string, ConfigValue>::const_iterator iter = values.find(key);
		if(iter != values.end())
			return (*iter).second.f;
		return default_value;
	}


	Color get_color(std::string key, Color default_value = 0x00000000) const {
		std::map<std::string, ConfigValue>::const_iterator iter = values.find(key);
		if(iter != values.end())
			return (*iter).second.c;
		return default_value;
	}


	std::vector<Color> get_gradient(std::string key) const {
		std::map<std::string, ConfigValue>::const_iterator iter = values.find(key);
		if(iter != values.end())
			return (*iter).second.g;
		return std::vector<Color>();
	}


	Configuration get_subconfig(std::string key_prefix) const {
		Configuration cfg;
		for(std::map<std::string, ConfigValue>::const_iterator iter = values.begin(); iter != values.end(); iter++)
			if((*iter).first.substr(0, key_prefix.length()) == key_prefix)
				cfg.set((*iter).first.substr(key_prefix.length()), (*iter).second);
		return cfg;
	}


	std::vector<Configuration> get_subconfigs(std::string key_prefix) const {
		size_t max_index = 0;
		for(std::map<std::string, ConfigValue>::const_iterator i = values.begin(); i != values.end(); i++) {
			if(i->first.substr(0, key_prefix.length() + 1) == key_prefix + ".") {
				long index = atol(i->first.substr(key_prefix.length() + 1).c_str());
				if(index > (long)max_index)
					max_index = index;
			}
		}

		std::vector<Configuration> cfgs;
		for(size_t i = 0; i <= max_index; i++) {
			char index[64];
			sprintf(index, "%zu", i);
			Configuration cfg = get_subconfig(key_prefix + "." + index + ".");
			if(cfg.empty())
				continue;
			cfgs.push_back(cfg);
		}
		return cfgs;
	}


	/*void dump() const {
		for(std::map<std::string, ConfigValue>::const_iterator iter = values.begin(); iter != values.end(); iter++)
			printf("%s = %s [int: %d]\n", (*iter).first.c_str(), (*iter).second.s.c_str(), (*iter).second.i);
	}*/


private:
	std::map<std::string, ConfigValue> values;
};


namespace Config {
	Configuration global_config;


	void set(std::string key, std::string value) {
		global_config.set(key, value);
	}


	bool isset(std::string key) {
		return global_config.isset(key);
	}


	std::string get(std::string key, std::string default_value = "") {
		return global_config.get(key, default_value);
	}


	bool get_bool(std::string key, bool default_value = false) {
		return global_config.get_bool(key, default_value);
	}


	int get_int(std::string key, int default_value = 0) {
		return global_config.get_int(key, default_value);
	}


	float get_float(std::string key, float default_value = 0.0) {
		return global_config.get_float(key, default_value);
	}


	Color get_color(std::string key, Color default_value = 0x00000000) {
		return global_config.get_color(key, default_value);
	}


	std::vector<Color> get_gradient(std::string key) {
		return global_config.get_gradient(key);
	}


	void load_arguments(int argc, char ** argv) {
		for(int i = 1; i < argc; i++) {
			if(argv[i][0] != '-') {
				char * arg = argv[i];
				char * key = arg;
				arg = strpbrk(arg, " \t=");
				if(arg == NULL) {
					global_config.set(key, "true");
					continue;
				}
				arg[0] = '\0';
				arg++;
				arg += strspn(arg, " \t=");
				char * value = arg;
				global_config.set(key, value);
			}
		}
	}


	bool load_file(const char * path) {
		FILE *file = fopen(path, "rt");
		if(file == NULL)
			return false;

		int line_number;
		char buffer[512];
		for(line_number = 1; fgets(buffer, sizeof(buffer) - 1, file) != NULL; line_number++) {
			char *line = buffer;

			// strip leading spaces
			while(line[0] == ' ' || line[0] == '\t')
				line++;

			// strip comments and trailing spaces
			char * end = strstr(line, "//");
			if(end == NULL)
				end = line + strlen(line);
			while(end > line && (end[-1] == ' ' || end[-1] == '\t' || end[-1] == '\r' || end[-1] == '\n'))
				end--;
			end[0] = '\0';

			// ignore empty lines
			if(line[0] == '\n' || line[0] == '\0')
				continue;

			// handle includes
			if(strncmp(line, "include", 7) == 0 && (line[7] == ' ' || line[7] == '\t')) {
				line += 8;
				while(line[0] == ' ' || line[0] == '\t')
					line++;
				load_file(line);
				continue;
			}

			// read <key>[ \t=]+<value> pair
			char * key = line;
			line = strpbrk(key, " \t=");
			line[0] = '\0';
			line++;
			line += strspn(line, " \t=");
			const char * value = line;

			if(strlen(value) == 0)
				global_config.set(key, "true");
			else
				global_config.set(key, value);
		}

		fclose(file);
		return true;
	}
};


#endif
